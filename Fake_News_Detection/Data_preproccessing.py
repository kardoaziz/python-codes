
# import os
import mysql.connector
import pandas as pd
import csv
import numpy as np
import nltk
from nltk.stem import SnowballStemmer
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import word_tokenize
import seaborn as sb
import AllData as stopwords
from dilman_py.ksscs import KurdishSoraniSpellCheker
from temp_py import postagging_using_search as pos

# before reading the files, setup the working directory to point to project repo
# reading data files
kssc = KurdishSoraniSpellCheker()

mydb = mysql.connector.connect(
    host="mysql.gb.stackcp.com",
    user="torbend-3231318204",
    password="vxwhyj8shy",
    database="torbend-3231318204",
    port="59054"
)

fakenews = []
fakenews_labels = []

# load fake news
def getFakeNews():
    mycursor = mydb.cursor()
    mycursor.execute("select content from fake_news ")
    result = mycursor.fetchall()  # fetches all the rows in a result set
    for i in result:
        fakenews.append(kssc.correction(i[0].decode('utf-8').strip()))
        fakenews_labels.append('false')
    print(len(fakenews))
    print(len(fakenews_labels))

truenews = []
truenews_labels = []

#load true news
def getTrueNews():
    mycursor = mydb.cursor()
    mycursor.execute("select cleaned_details from new_all_news_withdot ")
    result = mycursor.fetchall()  # fetches all the rows in a result set
    for i in result:
        truenews.append(kssc.correction(i[0].decode('utf-8').strip()))
        truenews_labels.append('true')
    print(len(truenews))
    print(len(truenews_labels))

getFakeNews()
getTrueNews()

# convert to fake news data frame
fake_df = pd.DataFrame(list(zip(fakenews, fakenews_labels)),
               columns =['Statement', 'Label'])

# convert to true news data frame
true_df = pd.DataFrame(list(zip(truenews, truenews_labels)),
               columns =['Statement', 'Label'])

# get n random true news from all true news where n= length of fake news
true_df=true_df.sample(n=len(fakenews))

# concat the fake news and true news
allnews=pd.concat([fake_df,true_df])

# shuffle the data
allnews=allnews.sample(frac=1)

#reset index from 0 to last index
allnews=allnews.reset_index(drop=True)

# print(len(allnews))

size=len(allnews)

# split data into train, test and validation
train_news = allnews.sample(n=int(size*0.8))
test_news = allnews.sample(n=int(size*0.15))
valid_news = allnews.sample(n=int(size*0.05))



def remove_stopword(data):
    for i in range(0,len(data)):
        data[i] = ' '.join(["" if j in stopwords.list_of_stop_words else j for j in data[i].split()])

    return data

def stem_to_vbgs(data):
    for i in range(0,len(data)):
        data[i] = pos.verb_to_vbgs(data[i])

    return data

# remove stopwords
train_news = remove_stopword(train_news)
test_news = remove_stopword(train_news)
valid_news = remove_stopword(train_news)

# stem to vbgs
train_news = stem_to_vbgs(train_news)
test_news = stem_to_vbgs(train_news)
valid_news = stem_to_vbgs(train_news)

print(len(train_news))
print(len(test_news))
print(len(valid_news))

# test_filename = 'test_kurdi.csv'
# train_filename = 'train_kurdi.csv'
# valid_filename = 'valid_kurdi.csv'
#
# train_news = pd.read_csv(train_filename)
# test_news = pd.read_csv(test_filename)
# valid_news = pd.read_csv(valid_filename)
#
# print(type(train_news))


# data observation
def data_obs():
    print("training dataset size:")
    print(train_news.shape)
    print(train_news.head(10))

    # below dataset were used for testing and validation purposes
    print(test_news.shape)
    print(test_news.head(10))

    print(valid_news.shape)
    print(valid_news.head(10))


# check the data by calling below function
# data_obs()

# distribution of classes for prediction
def create_distribution(dataFile):
    return sb.countplot(x='Label', data=dataFile, palette='hls')


# by calling below we can see that training, test and valid data seems to be failry evenly distributed between the classes
# create_distribution(train_news)
# create_distribution(test_news)
# create_distribution(valid_news)


# data integrity check (missing label values)
# none of the datasets contains missing values therefore no cleaning required
def data_qualityCheck():
    print("Checking data qualitites...")
    train_news.isnull().sum()
    train_news.info()

    print("check finished.")

    # below datasets were used to
    test_news.isnull().sum()
    test_news.info()

    valid_news.isnull().sum()
    valid_news.info()



# creating ngrams
# unigram
def create_unigram(words):
    assert type(words) == list
    return words


# bigram
def create_bigrams(words):
    assert type(words) == list
    skip = 0
    join_str = " "
    Len = len(words)
    if Len > 1:
        lst = []
        for i in range(Len - 1):
            for k in range(1, skip + 2):
                if i + k < Len:
                    lst.append(join_str.join([words[i], words[i + k]]))
    else:
        # set it as unigram
        lst = create_unigram(words)
    return lst


"""
#trigrams
def create_trigrams(words):
    assert type(words) == list
    skip == 0
    join_str = " "
    Len = len(words)
    if L > 2:
        lst = []
        for i in range(1,skip+2):
            for k1 in range(1, skip+2):
                for k2 in range(1,skip+2):
                    for i+k1 < Len and i+k1+k2 < Len:
                        lst.append(join_str.join([words[i], words[i+k1],words[i+k1+k2])])
        else:
            #set is as bigram
            lst = create_bigram(words)
    return lst
"""










