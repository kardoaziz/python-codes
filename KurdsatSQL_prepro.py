# coding=utf8
import codecs
import re
import csv
# from bs4 import BeautifulSoup
from klpt.preprocess import Preprocess

import re
import pyodbc
from klpt.stem import Stem

to_be_removed = ['؛', 'K', 'N', ':', '-', '_', '&', ')', '(', '=', '؟', '?', '!', '%', '@', '/', '*', 'ص', 'l', 'r',
                 'm', ';', '$', '€', 'p', 'o', 'u', 'n', 'd', '{', 'ذ', '#', 'E', 'D', 'i', 'k', 'a', 'G', 'e', 's',
                 'W', 'f', 't', 'R', 'c', 'h', 'H', 'ط', 'U', 'L', 'V', 'O', '+', 'C', 'A', 'M', 'B', 'v', 'b', 'x',
                 'Ç', 'Y', 'g', 'P', 'ü', 'z', 'S', "'", 'T', 'Z', 'F', '٪', 'ظ', 'I', 'q', 'w', 'y', 'ْ', 'X', 'Q',
                 'ٲ', '«', '»', '\\', '[', 'ث', 'j', 'ı', '１', '２', '３', '４', '５', '６', '７', 'ء', '>', '<', 'ھ', '٫',
                 'ض', '\u202c', 'J', 'ê', '۔', '}', '８', '９', '０', 'е', 'о', 'а', '٬', 'Р', 'И', 'А', 'Н', 'в', 'с',
                 'т', 'и', '👇', '\u202a', 'ä', ']', 'ö', 'İ', '·', 'î', '�', 'ş', '☀', '️', 'ç', 'Ö', 'û', 'Ş', '🙏',
                 '🏽', '\u202b', '🇸', '🇾', 'ٸ', '￼', 'ٔ', '؍', '✅', '`', '~', '٭', '\xad', '|', '¾',]
preprocessor_ckb = Preprocess("Sorani", "Arabic", numeral="Latin")
stemmer = Stem("Sorani", "Arabic")

import alphabets

alphas = ["ا", "ب", "ج", "چ", "د", "ە", "ێ", "ف", "گ", "ه", "ح", "ژ", "ک", "ل", "ڵ", "م", "ن", "ۆ", "پ", "ق", "ر", "ڕ",
          "س", "ش", "ت", "وو", "ڤ", "خ", "ز", "غ", "ع", "ئ", " ", "و", "ی", "0", "1", "2", "3", "4", "5", "6", "7", "8",
          "9", "10", 'ذ', 'ص', 'ض', 'ھ', 'ث', 'ظ', 'ط']


def cleaning(text):
    cleaned = preprocessor_ckb.normalize(text)
    # remove url and links in the content
    cleaned = re.sub(r'http\S+', '', cleaned)
    # remove some special characters
    cleaned = cleaned.strip()
    cleaned = cleaned.replace("\n", " ")
    cleaned = cleaned.replace("\r", "")
    # cleaned = cleaned.replace(".", " ")
    # cleaned = cleaned.replace("،", " ")
    # cleaned = cleaned.replace(".", " ")
    # cleaned = cleaned.replace(",", " ")
    # a = a.replace("\"", "")
    while "  " in cleaned:
        cleaned = cleaned.replace("  ", " ")
    # a = a.replace("  ", " ")
    to_be_removed = []
    # # filter the characters and keep only those that are in alphas
    # # add characters in a list
    for l in cleaned:
        if l not in alphas and l not in to_be_removed:
            to_be_removed.append(l)

    # replacing those characters with ''
    for l in to_be_removed:
        cleaned = cleaned.replace(l, "")

    cleaned = preprocessor_ckb.normalize(cleaned)

    return cleaned


def getColoumns(index, update):
    conn = pyodbc.connect('Driver={SQL Server};'
                          'Server=PC2\SQLEXPRESS;'
                          'Database:kurdsat;'
                          'Trusted_Connection=yes;')

    conn2 = pyodbc.connect('Driver={SQL Server};'
                           'Server=PC2\SQLEXPRESS;'
                           'Database:kurdsat;'
                           'Trusted_Connection=yes;')

    conn.autocommit = True
    cursor = conn.cursor()

    conn2.autocommit = True
    cursor2 = conn2.cursor()



    sql = 'select * from [kurdsat].[dbo].[Content];'
    cursor.execute(sql)

    for i in cursor:
        try:
            # Get the Content by coloumn index and cleaning it
            _id = i[index[0]]
            print(_id)
            cleaned_title = cleaning(i[index[1]])
            cleaned_details = cleaning(i[index[2]])
            if update:
                cursor2.execute('''update [kurdsat].[dbo].[Content] set title=? where id=?''', cleaned_title, _id)
                cursor2.execute('''update [kurdsat].[dbo].[Content] set Detail=? where id=?''', cleaned_details, _id)

        except Exception as e:
            print(e)
    cursor.commit()
    cursor.close()
    conn.close()

    cursor2.commit()
    cursor2.close()
    conn2.close()


getColoumns([2, 0, 1], True)
