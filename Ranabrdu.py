import mysql.connector
from Word import Word
# import generate_sada_tofiq
import json

mydb = mysql.connector.connect(
    host="mysql.gb.stackcp.com",
    user="torbend-3231318204",
    password="vxwhyj8shy",
    database="torbend-3231318204",
    port="59054"
)

all_words = []


def getallgeneratedwords():
    mycursor = mydb.cursor()
    mycursor.execute("select * from generated_words  ")
    result = mycursor.fetchall()  # fetches all the rows in a result set
    for i in result:
        all_words.append(i[1])
    print(len(all_words))


def getPronounnRanabrdu(type_tepar):
    pronoun_tenapar = '{"1t":"م","1k":"مان","2t":"ت","2k": "تان","3t": "ی","3k":"یان"}'
    pronoun_tepar = '{"1t": "م","1k": "ین","2t": "یت","2k": "ن","3t": "ێت","3k": "ن"}'
    pronouns = pronoun_tenapar if type_tepar == "tn" else pronoun_tepar
    pronoun_dict = json.loads(pronouns)
    return pronoun_dict


def ranabrdui_sada_darezhraw(negative="np", sql="", save=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='d' and deleted_at is null  "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix_neg = "نا"
    prefix = ["دە", "ئە"]
    k = 1
    for i in result:
        print(k)
        k+=1
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0])
        print(word.root)
        # print(word.type_tepar)
        tenses = []
        neg_tenses = []
        pronouns = getPronounnRanabrdu("t")
        barkars = getPronounnRanabrdu("tn")
        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    for pre in prefix:
                        if word.type_tepar == "t":
                            if j2[0] != j[0]:
                                bbk = barkars[j2]
                            else:
                                continue
                            if i[13] != 1:
                                bbk = ''
                            if word.root.count("_") == 3:
                                g_word = word.replace(word.root, '_', bbk, 0)
                                g_word = word.replace(g_word, '_', pre, -2)
                                g_word = word.replace(g_word, '_', pronouns[j], -1)
                            elif word.root[0] == '_':
                                g_word = word.replace(word.root, '_', pre + bbk, 0)
                                g_word = word.replace(g_word, '_', pronouns[j], -1)
                            else:
                                g_word = word.replace(word.root, '_', bbk + pre, 0)
                                g_word = word.replace(g_word, '_', pronouns[j], -1)
                        else:
                            g_word = word.replace(word.root, '_', pronouns[j], -1)
                            g_word = word.replace(g_word, '_', pre, 0)
                        g_word = word.cleanit(g_word)
                        if g_word not in all_words:
                            barkar = ''
                            if bbk != '':
                                barkar = j2
                            all_words.append(g_word)
                            tenses.append((g_word, j, barkar))
        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        # bo check krdn agar jenawe barkar wardagret yaxwd na
                        if i[13] != 1:
                            bbk = ''
                        if word.root.count('_') > 2:
                            g_word = word.replace(word.root, '_', bbk, 0)
                            g_word = word.replace(word.root, '_', prefix_neg, -2)
                            g_word = word.replace(g_word, '_', pronouns[j], -1)
                        elif word.root[0] == '_':
                            g_word = word.replace(word.root, '_', prefix_neg + bbk, 0)
                            g_word = word.replace(g_word, '_', pronouns[j], -1)
                        else:
                            g_word = word.replace(word.root, '_', bbk + prefix_neg, 0)
                            g_word = word.replace(g_word, '_', pronouns[j], -1)
                    else:

                        g_word = word.replace(word.root, '_', prefix_neg, 0)
                        g_word = word.replace(g_word, '_', pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save == 1:
            word.saveword(tenses, "ڕانەبردووی سادە", 0, "داڕێژراو")
            word.saveword(neg_tenses, "ڕانەبردووی سادە", 1, "داڕێژراو")


def ranabrdui_sada_lekdraw(negative="np", sql="" ,save=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='l' and deleted_at is null  "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix_neg = "نا"
    prefix = ["دە", "ئە"]
    k = 1;
    for i in result:
        print(i[0])
        k +=1
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0])
        print(word.root)
        # print(word.type_tepar)
        tenses = []
        neg_tenses = []
        pronouns = getPronounnRanabrdu("t")
        barkars = getPronounnRanabrdu("tn")
        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    for pre in prefix:
                        if word.type_tepar == "t":
                            if j2[0] != j[0]:
                                bbk = barkars[j2]
                            else:
                                continue
                            if i[13] != 1:
                                bbk = ''
                            if word.root.count("_") == 3:
                                g_word = word.replace(word.root, '_', bbk, 0)
                                g_word = word.replace(g_word, '_', pre, -2)
                                g_word = word.replace(g_word, '_', pronouns[j], -1)
                            elif word.root[0] == '_':
                                g_word = word.replace(word.root, '_', pre + bbk, 0)
                                g_word = word.replace(g_word, '_', pronouns[j], -1)
                            else:
                                g_word = word.replace(word.root, '_', bbk + pre, 0)
                                g_word = word.replace(g_word, '_', pronouns[j], -1)
                        else:
                            g_word = word.replace(word.root, '_', pronouns[j], -1)
                            g_word = word.replace(g_word, '_', pre, 0)
                        g_word = word.cleanit(g_word)
                        if g_word not in all_words:
                            barkar = ''
                            if bbk != '':
                                barkar = j2
                            all_words.append(g_word)
                            tenses.append((g_word, j, barkar))
        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        # bo check krdn agar jenawe barkar wardagret yaxwd na
                        if i[13] != 1:
                            bbk = ''
                        if word.root.count('_') > 2:
                            g_word = word.replace(word.root, '_', bbk, 0)
                            g_word = word.replace(word.root, '_', prefix_neg, -2)
                            g_word = word.replace(g_word, '_', pronouns[j], -1)
                        elif word.root[0] == '_':
                            g_word = word.replace(word.root, '_', prefix_neg + bbk, 0)
                            g_word = word.replace(g_word, '_', pronouns[j], -1)
                        else:
                            g_word = word.replace(word.root, '_', bbk + prefix_neg, 0)
                            g_word = word.replace(g_word, '_', pronouns[j], -1)
                    else:

                        g_word = word.replace(word.root, '_', prefix_neg, 0)
                        g_word = word.replace(g_word, '_', pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save == 1:
            word.saveword(tenses, "ڕانەبردووی سادە", 0, "لێکدراو")
            word.saveword(neg_tenses, "ڕانەبردووی سادە", 1, "لێکدراو")


def ranabrdu_bkar_nadyar(negative="np", sql="", save=0):
    shazakan = [
        "تۆڵەسەندنەوە",
        "ستەملێکردن",
        "قفڵلێدان",
        "درکپێکردن",
        "قێزلێکردنەوە",
        "وازلێهێنان",
        "پەرەپێسەندن",
        "لەسەرداخستن",
        "چاوتێبڕین",
        "بۆدەرکەوتن",
        "پێباشبوون",
        "ئاوڕۆێدانەوە",
        "خۆراکپێدان",
        "سەرلێدان",
        "تۆڵەکردنەوە",
        "سەرپەڕاندن",
        "چاولێپۆشین",
        "دەستلێدان",
        "دەتتێوەردان",
        "قەرزپێدان",
        "خەوزڕاندن",
        "نانپيدان",
        "دانپێدان",
        "ڕێگەپێدان",
        "دڵشکاندن",
        "جوڵەپێکردن",
        "تەقەلێکردن",
        "قۆڵبڕین",
        "خۆپیشاندان",
        "ڕێگاپێدان",
        "مەشقپێکردن",
        "گڕتێبەربوون",
        "پێڕاگەیاندن",
        "گاڵتەپێکردن",
        "دەستتێکەڵکردن",
        "گەشەپيدان",
        "زۆرلێکردن",
        "ڕێگەپێدان",
        "دانپیانان",
        "کۆتاییپێهێنان",
        "چاوداگرتن",
        "پەلەپیتکەلێدان",
        "تەقەڵلێدان",
        "دەستپێداهێنان",
        "بوارپێدان",
        "لەگەڵبوون",
        "جڵەوڕاکێشان",
        "زمانگرتن",
        "بەسەرهێنان",
        "هێزبڕین",
        "ماوەپێدان",
        "پەلەلێکردن",
        "ڕێپێدان",
        "بەبیرهاتن",
        "یەخەگرتن",
        "دڵفڕین",
        "باوەشپێداکردن",
        "ڕووپۆشین",
        "کاژداماڵین",
        "لەبەرکردن",
        "خەمڕەواندن",
        "خۆلێگێلکردن",
        "پەرەپێدان",
        "باسکردن",
        "درێژەپێدان",
        "کێشەلابردن",
        "دەستتێوەردان",
        "کەڵەشاخلێگرتن",
        "چاولێکردن",
        "نازپێدان",
        "شوێنگرتن",
        "بەرگتێگرتن",
        "ئاگرتێبەردان",
        "درێژەپێدان",
        "دەستپێکردن",
        "بەرگرتن",
        "سەربڕین",
        "بەسەرهێنان",
        "بۆنخۆشکردن",
        "هەستپێکردن",
        "دڵبردن",
        "زمانگرتن",
        "بیرلێکردنەوە",
        "سەرسوڕماندن",
        "ڕووتێکردن",
        "قینهەڵساندن",
        "ئاوتێوەردان",
        "بەدواداناردن",
        "شرینقەلێدان",
        "دەرزیلێدان",
        "ڕێزگرتن",
        "ماڵبڕین",
        "گوێگرتن",
        "سەرتاشین",
        "لاساییکردنەوە",
        "کارڕاپەڕاندن",
        "بریقەپێدان",
        "ڕاوەرگرتن",
        "نقورچلێدان",
        "گیرفانبڕین",
        "تەماشاکردن",
        "کانزالێدان",
        "پڕپیاکردن",
        "ئارەزووجوڵاندن",
        "لێوونبوون",
        "شوێنگۆڕین",
        "پشتگرتن",
        "شێوەپێدان",
        "شکڵپێدان",
        "نەفرەتلێکردن",
        "داوایلێبوردنلێکردن",
        "باوەڕپێهێنان",

    ]
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='l' and type_tepar='t' and deleted_at is null  "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix_neg = "نا"
    prefix = ["دە", "ئە"]
    k =1
    for i in result:
        print(k)
        k+=1
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0])
        print(word.root)
        print(word.root2)
        # print(word.type_tepar)
        tenses = []
        shaz  = False
        neg_tenses = []
        pronouns = getPronounnRanabrdu("t") if i[13] == 1 and word.word not in shazakan else getPronounnRanabrdu("tn")
        # barkars =  getPronounnRanabrdu("tn") if word.word in shazakan else getPronounnRanabrdu("t")
        if  word.word in shazakan:
            shaz = True
        #     print(pronouns)
        #     return "STOPPPPP"
        if negative.find('p') != -1:
            for j in pronouns:
                for pre in prefix:
                    if True:
                        if i[13] == 1:
                            if word.root2 is not None and len(word.root2) > 0:
                                if shaz:
                                    if word.root2.count("_") == 3:
                                        g_word = word.replace(word.root2, '_', pronouns[j], 0)
                                        g_word = word.replace(g_word, '_', pre, -2)
                                        g_word = word.replace(g_word, '_', 'ێت', -1)
                                    else:
                                        g_word = word.replace(word.root2, '_', pronouns[j]+pre, 0)
                                        # g_word = word.replace(g_word, '_', pre, -2)
                                        g_word = word.replace(g_word, '_', 'ێت', -1)
                                else:
                                    if word.root2.count("_") == 3:
                                        g_word = word.replace(word.root2, '_', pronouns[j], 0)
                                        g_word = word.replace(g_word, '_', pre, -2)
                                        g_word = word.replace(g_word, '_', 'ێت', -1)
                                    else:
                                        g_word = word.replace(word.root2, '_', pre, 0)
                                        g_word = word.replace(g_word, '_', 'ێ' + pronouns[j], -1)
                            else:
                                if shaz:
                                    if word.root.count("_") == 3:
                                        g_word = word.replace(word.root, '_', pronouns[j], 0)
                                        g_word = word.replace(g_word, '_', pre, -2)
                                        g_word = word.replace(g_word, '_', 'ێت', -1)
                                    else:
                                        g_word = word.replace(word.root, '_', pronouns[j]+pre, 0)
                                        # g_word = word.replace(g_word, '_', pre, -2)
                                        g_word = word.replace(g_word, '_', 'ێت', -1)
                                else:
                                    if word.root.count("_") == 3:
                                        g_word = word.replace(word.root, '_', pronouns[j], 0)
                                        g_word = word.replace(g_word, '_', pre, -2)
                                        g_word = word.replace(g_word, '_', 'ێت', -1)
                                    else:
                                        g_word = word.replace(word.root, '_', pre, 0)
                                        g_word = word.replace(g_word, '_', 'ێ' + pronouns[j], -1)
                        else:
                            if word.root2 is not None and len(word.root2) > 0:
                                # g_word = word.replace(word.root2, '_', pronouns[j], 0)
                                # print("can not barkar <=-======")
                                g_word = word.replace(word.root2, '_', pre, -2)
                                g_word = word.replace(g_word, '_', 'ێت', -1)
                                # print(g_word)
                            else:
                                if word.root.count('_') < 2:
                                    # g_word =  word.replace(word.root, '_', , -2)
                                    g_word = pre+ word.replace(g_word, '_', 'ێت', 0)
                                else:
                                    # print("can not barkar <=-======")
                                    g_word = word.replace(word.root, '_', pre, -2)
                                    g_word = word.replace(g_word, '_', 'ێت', -1)
                                    # print(g_word)
                    else:
                        continue
                    g_word = word.cleanit(g_word)

                    all_words.append(g_word)
                    tenses.append((g_word, '', j))
        if negative.find('n') != -1:
            for j in pronouns:
                # for pre in prefix:
                if True:
                    if i[13] == 1:
                        if word.root2 is not None and len(word.root2) > 0:
                            if shaz:
                                if word.root2.count("_") == 3:
                                    g_word = word.replace(word.root2, '_', pronouns[j], 0)
                                    g_word = word.replace(g_word, '_', prefix_neg, -2)
                                    g_word = word.replace(g_word, '_', 'ێت', -1)
                                else:
                                    g_word = word.replace(word.root2, '_', pronouns[j] + prefix_neg, 0)
                                    # g_word = word.replace(g_word, '_', pre, -2)
                                    g_word = word.replace(g_word, '_', 'ێت', -1)
                            else:
                                if word.root2.count("_") == 3:
                                    g_word = word.replace(word.root2, '_', pronouns[j], 0)
                                    g_word = word.replace(g_word, '_', prefix_neg, -2)
                                    g_word = word.replace(g_word, '_', 'ێت', -1)
                                else:
                                    g_word = word.replace(word.root2, '_', prefix_neg, 0)
                                    g_word = word.replace(g_word, '_', 'ێ' + pronouns[j], -1)
                        else:
                            if shaz:
                                if word.root.count("_") == 3:
                                    g_word = word.replace(word.root, '_', pronouns[j], 0)
                                    g_word = word.replace(g_word, '_', prefix_neg, -2)
                                    g_word = word.replace(g_word, '_', 'ێت', -1)
                                else:
                                    g_word = word.replace(word.root, '_', pronouns[j] + prefix_neg, 0)
                                    # g_word = word.replace(g_word, '_', prefix_neg, -2)
                                    g_word = word.replace(g_word, '_', 'ێت', -1)
                            else:
                                if word.root.count("_") == 3:
                                    g_word = word.replace(word.root, '_', pronouns[j], 0)
                                    g_word = word.replace(g_word, '_', prefix_neg, -2)
                                    g_word = word.replace(g_word, '_', 'ێت', -1)
                                else:
                                    g_word = word.replace(word.root, '_', prefix_neg, 0)
                                    g_word = word.replace(g_word, '_', 'ێ' + pronouns[j], -1)
                    else:
                        if word.root2 is not None and len(word.root2) > 0:
                            # g_word = word.replace(word.root2, '_', pronouns[j], 0)
                            # print("can not barkar <=-======")
                            g_word = word.replace(word.root2, '_', prefix_neg, -2)
                            g_word = word.replace(g_word, '_', 'ێت', -1)
                            # print(g_word)
                        else:
                            if word.root.count('_') < 2:
                                # g_word =  word.replace(word.root, '_', , -2)
                                g_word = pre + word.replace(g_word, '_', 'ێت', 0)
                            else:
                                # print("can not barkar <=-======")
                                g_word = word.replace(word.root, '_', prefix_neg, -2)
                                g_word = word.replace(g_word, '_', 'ێت', -1)
                                # print(g_word)
                else:
                    continue
                g_word = word.cleanit(g_word)
                all_words.append(g_word)
                neg_tenses.append((g_word, '', j))
        print(tenses)
        print(neg_tenses)
        if save == 1:
            word.saveword(tenses, "ڕانەبردووی بکەر نادیار", 0, "لێکدراو")
            word.saveword(neg_tenses, "ڕانەبردووی بکەر نادیار", 1, "لێکدراو")

def chawg(negative="np", sql="", save =0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where deleted_at is null  "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix_neg = "نە"
    k = 1;
    for i in result:
        print(i[0])
        k +=1
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0],brga=i[14])

        print(word.brga)
        # print(word.type_tepar)
        tenses = []
        neg_tenses = []

        if negative.find('p') != -1:
           if(word.type=='s'):
               g_word = word.word
           else:
               g_word = word.replace(word.brga, '_', '', -1)
           # g_word = word.replace(word.brga, '_', '', -1)
           g_word = word.cleanit(g_word)
           tenses.append((g_word, '', ''))
        if negative.find('n') != -1:
            if (word.type == 's'):
                g_word = prefix_neg+g_word
            else:
               g_word = word.replace(word.brga, '_', prefix_neg, -1)
            g_word = word.cleanit(g_word)
            neg_tenses.append((g_word, '', ''))
        if word.type=="s":
            typee = "سادە"
        elif word.type=="d":
            typee = "داڕێژراو"
        else:
            typee = "لێکدراو"
        print(tenses)
        print(neg_tenses)
        if save == 1:
            word.saveword(tenses, "چاوگ", 0, typee)
            word.saveword(neg_tenses, " چاوگ", 1,typee)

getallgeneratedwords()
chawg("pn", "", 1)
# saveit = 0
