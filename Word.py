import re
import mysql.connector
mydb = mysql.connector.connect(
    host="mysql.gb.stackcp.com",
    user="torbend-3231318204",
    password="vxwhyj8shy",
    database="torbend-3231318204",
    port="59054")
# all_words = []
class Word:


    def __init__(self, _word="", _root="", _base="", _type="", _type_tepar="", _root2="", _root3="",_id_vbgs="",brga=""):
        self.word = _word.replace("ک", "ک")
        self.root = _root.replace("ک", "ک")
        self.base = _base.replace("ک", "ک")
        self.type = _type
        self.type_tepar = _type_tepar
        self.root2 = _root2
        self.root3 = _root3
        self.id_vbgs = _id_vbgs
        self.brga = brga


    def replace(self, text, from_char, to_char, index):
        if text.find(from_char) != -1:
            underscore_index = [m.start() for m in re.  finditer(from_char, text)]
            return text[:underscore_index[index]] + to_char + text[underscore_index[index] + 1:]
        else:
            return text
    # check if the word ends with any of the character in the list_of_chars and skips the characters in the second
    # parameter


    def cleanit(self,text):
        underscore_index = [m.start() for m in re.finditer("_", text)]
        for i in underscore_index:
            if i>0 and i!=len(text)-1:
                if text[i-1] == "ا" and text[i+1]=="ە":
                    text = text[:i]+ "ی" + text[i+1:]

        for c in range(len(text)-1):
            if c<len(text) and c+1<len(text) and text[c]=="ا" and text[c+1]== "ە":
                text = text[:c+1] + "ی" + text[c+1:]
            elif c<len(text) and c+1<len(text) and text[c]=="ا" and text[c+1]== "ا":
                text = text[:c] + "" + text[c+1:]
            elif c<len(text) and c+1<len(text) and  text[c]=="ە" and text[c+1]=="ە":
                text = text[:c] + "" + text[c + 1:]

                # print("got it")

        text = text.replace("_", "")
        text = text.replace("ییی", "یی")
        text = text.replace("دەێێ", "دێ")
        text = text.replace("ئەێێ", "یە")
        text = text.replace("ێێ", "ێ")
        text = text.replace("ەێ", "ا")
        text = text.replace("ۆێ", "وا")
        text = text.replace("رر", "ر")
        return text


    def endswith(self, text, list_of_chars, skip_chars):
        for c in skip_chars:
            text = text.replace(c, "")
        return list(filter(text.endswith, list_of_chars)) != []

    # def getallgeneratedwords(self):
    #     mycursor = mydb.cursor()
    #     mycursor.execute("select * from generated_words  ")
    #     result = mycursor.fetchall()  # fetches all the rows in a result set
    #     for i in result:
    #          all_words.append(i[1])
    #     print(len(all_words))

    def saveword(self, words, tense, is_negative,type="سادە"):
        sql = "INSERT INTO generated_words ( word, id_vbgs,tense,is_negative,type,bkar,barkar) VALUES (%s, %s,%s,%s,%s,%s,%s)"
        # all_words=[]
        # mycursor = mydb.cursor()
        # mycursor.execute("select * from generated_words  ")
        # result = mycursor.fetchall()  # fetches all the rows in a result set
        # for i in result:
        #     all_words.append(i[1])
        # # print(len(all_words))
        # mycursor.close()
        mycursor = mydb.cursor()

        for word in words:

            # mycursor.execute("select * from generated_words where  word=%s and id>%s", (word, 0))
            # result = mycursor.fetchall()
            #
            # if(len(result)==0):
            #     # print(word, " is not exist")
            # print(word[0])
            # print(word[1])
            # print(word[2])
            # if word not in all_words:
            val = (word[0], self.id_vbgs, tense, is_negative,type,word[1],word[2])
            mycursor.execute(sql, val)
        mydb.commit()
        mycursor.close()
