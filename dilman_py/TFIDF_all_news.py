import pandas as pd
import mysql.connector

from sklearn.feature_extraction.text import TfidfVectorizer

# From: https://towardsdatascience.com/tf-idf-explained-and-python-sklearn-implementation-b020c5e83275

def get_all_news():
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()

    details = []
    ids = []
    sql = 'SELECT * FROM tbl_all_news'
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    for news in myresult:
        details.append(news[2])
        ids.append(news[0])
    mydb.close()

    return [ids, details]

def get_unique_words():
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()

    sql = 'SELECT * FROM news_word_freq_cleaned'
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mydb.close()
    return myresult

all_news = get_all_news()
unique_words = get_unique_words()

# V1
tfIdfVectorizer=TfidfVectorizer(use_idf=True)
tfIdf = tfIdfVectorizer.fit_transform(all_news)
df = pd.DataFrame(tfIdf[0].T.todense(), index=tfIdfVectorizer.get_feature_names(), columns=["TF-IDF"])
df = df.sort_values('TF-IDF', ascending=False)

print (df)


# tfidf_vectorizer = TfidfVectorizer()
# tfidf_vector = tfidf_vectorizer.fit_transform(all_news[1])
# tfidf_df = pd.DataFrame(tfidf_vector.toarray(), index=all_news[0], columns=tfidf_vectorizer.get_feature_names())
# tfidf_df.loc['00_Document Frequency'] = (tfidf_df > 0).sum()
#




