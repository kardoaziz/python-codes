import pyodbc
import mysql.connector

def get_all_cleaned_news():
    conn = pyodbc.connect('Driver={SQL Server};'
                          'Server=PC;'
                          'Database:[all_news];'
                          'Trusted_Connection=yes;')
    conn.autocommit = True
    cursor = conn.cursor()

    sql = 'select * from [all_news].[dbo].[tbl_all_news_cleaned];'
    cursor.execute(sql)

    news = []
    for row in cursor:
        try:
            news.append([row[0], row[1], row[2]])
        except Exception as e:
            print(e)
    return news

def insert_to_mysql_db(all_news):
    mydb = mysql.connector.connect(user='root', password='root',
                                       host='127.0.0.1',
                                       database='all_news')
    mycursor = mydb.cursor()

    sql = "INSERT INTO all_news_cleaned (id, title, details) VALUES (%s, %s,%s)"
    for news in all_news:
        val = (news[0], news[1], news[2])
        mycursor.execute(sql, val)
        mydb.commit()

news=get_all_cleaned_news()
insert_to_mysql_db(news)