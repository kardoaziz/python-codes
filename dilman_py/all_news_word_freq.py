import mysql.connector

def get_all_words():
    mydb = mysql.connector.connect(user='root', password='root',
                                   host='127.0.0.1',
                                   database='all_news')
    mycursor = mydb.cursor()
    sql = 'SELECT * from word_freq'
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mydb.close()
    return myresult

def insert_into_online_db(words):
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()

    sql = "INSERT INTO news_word_freq (word, freq) VALUES (%s, %s)"
    for word in words:
        val = (word[1], word[2])
        mycursor.execute(sql, val)
        mydb.commit()

# print('geeting words')
# words = get_all_words()
#
# print('inserting words')
# insert_into_online_db(words)

print('getting words from IQ server...')


def get_all_words_from_iq_db():
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()

    sql = 'SELECT * from news_word_freq_clean'
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mydb.close()
    return myresult

def remove_duplicates(all_words):
    d = {}
    for word in all_words:
        if d.__contains__(word[1]):
            d[word[1]] = d[word[1]] + word[2]
        else:
            d[word[1]] = word[2]

    return d


def delete_words(id=-1):
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()
    sql = 'DELETE from news_word_freq_clean where id >0'
    if id!=-1:
        sql = 'DELETE from news_word_freq_clean where id='+str(id)
    mycursor.execute('truncate news_word_freq_clean')
    mydb.close()


def insert_into_iq_db(unique_words):
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()

    sql = "INSERT INTO news_word_freq_clean (word, freq) VALUES (%s, %s)"
    for key in unique_words:
        val = (key, unique_words[key])
        mycursor.execute(sql, val)
        mydb.commit()

#
# print('getting all words')
# all_words = get_all_words_from_iq_db()
#
# print('getting duplicate words')
# unique_words  =remove_duplicates(all_words)
#
# print('deletting duplicate words')
# delete_words()
#
# print('inserting duplicate words into iq db')
# insert_into_iq_db(unique_words)






