import mysql.connector

def get_news(id=-1):
    mydb = mysql.connector.connect(user='root', password='root',
                                   host='127.0.0.1',
                                   database='all_news')
    mycursor = mydb.cursor()
    sql='SELECT * from all_news_cleaned'
    if id != -1:
        sql = 'SELECT * from all_news_cleaned where id ='+str(id)
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mydb.close()
    return myresult


def get_unique_words(id=-1):
    mydb = mysql.connector.connect(user='root', password='root',
                                   host='127.0.0.1',
                                   database='all_news')
    mycursor = mydb.cursor()
    sql = 'SELECT * from word_freq'
    if id != -1:
        sql = 'SELECT * from word_freq where id =' + str(id)
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mydb.close()
    return myresult

def get_unique_word_as_dic():

    uwd = {}
    mydb = mysql.connector.connect(user='root', password='root',
                                   host='127.0.0.1',
                                   database='all_news')
    mycursor = mydb.cursor()
    sql = 'SELECT * from word_freq'
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mydb.close()
    return myresult

def udpate_tbl(all_news):
    mydb = mysql.connector.connect(user='root', password='root',
                                   host='127.0.0.1',
                                   database='all_news')
    mycursor = mydb.cursor()
    for news in all_news:
        # sql = 'update all_news_cleaned set title='+news[1].decode('utf8')+', details='+news[2].decode('utf8')+' where id='+str(news[0])+';'

        sql = 'update all_news_cleaned set title=%s, details=%s where id=%s'
        val = (news[1].decode('utf8'), news[2].decode('utf8'), news[0])
        mycursor.execute(sql,val)
        mydb.commit()

def get_details(all_news):
    details = []
    for news in all_news:
        details.append(news[2].decode('utf8'))
    return details

def extract_unique_words(details):
    uniq_words= []
    for detail in details:
        uniq_words = set(uniq_words).union(set(detail.split(' ')))

    return uniq_words

def insert_into_db(unique_words):
    mydb = mysql.connector.connect(user='root', password='root',
                                       host='127.0.0.1',
                                       database='all_news')
    mycursor = mydb.cursor()

    sql = "INSERT INTO word_freq (word, freq) VALUES (%s,%s)"
    for uw in unique_words:
        val = (uw, 0)
        mycursor.execute(sql, val)
        mydb.commit()



def update_word_freq(id, new_freq):
    mydb = mysql.connector.connect(user='root', password='root',
                                   host='127.0.0.1',
                                   database='all_news')
    mycursor = mydb.cursor()
    sql = 'update word_freq set freq=%s where id=%s'
    unique_word = get_unique_words(id)
    val = (new_freq+unique_word[0][2], id)
    mycursor.execute(sql, val)
    mydb.commit()

def update_word_freq2(word_freq):
    mydb = mysql.connector.connect(user='root', password='root',
                                   host='127.0.0.1',
                                   database='all_news')
    mycursor = mydb.cursor()
    sql = 'update word_freq set freq=%s where id=%s'
    for wf in word_freq:
        val = (wf[1], wf[0])
        mycursor.execute(sql, val)
        mydb.commit()

def clculate_word_freq(details, unique_words):
    proccessed_news = 0
    for detail in details:
        proccessed_news = proccessed_news + 1
        if proccessed_news%100 == 0:
            print(proccessed_news)
        for unique_word in unique_words:
            count = detail.count(unique_word[1])
            update_word_freq(unique_word[0], count)

def clculate_word_freq2(details, unique_words):
    proccessed_news = 0
    wf=[]
    for unique_word in unique_words:
        count = 0
        proccessed_news = proccessed_news + 1
        if proccessed_news%100 == 0:
            print(proccessed_news)
        for detail in details:
            count = count+detail.count(unique_word[1])
        #update_word_freq(unique_word[0], count)
        wf.append([unique_word[0],count])

    return wf


print('getting all news')
all_news = get_news()
print('getting detials')
details=get_details(all_news)
# print('getting unique words')
# unique_words = extract_unique_words(details)
# print('inserting')
# insert_into_db(unique_words)

print('geting unique words')
unique_words = get_unique_words()

print('calculating word freq')
word_freq = clculate_word_freq2(details, unique_words)


update_word_freq2(word_freq)
print('done')

