# -*- coding: utf-8 -*-
import codecs

import mysql.connector

def get_all_news():
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()

    sql = 'SELECT * FROM tbl_all_news'
    mycursor.execute(sql)
    myresult = mycursor.fetchall()

    return myresult

def get_unique_words():
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()

    sql = 'SELECT * FROM news_word_freq_cleaned'
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mydb.close()
    return myresult

def update_db(id, count):
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()
    sql = "UPDATE news_word_freq_cleaned SET docs = %s where id = %s"
    val = (count, id)
    mycursor.execute(sql, val)
    mydb.commit()

def write_to_file(terms, counts):
    with codecs.open("test_output.txt", "w", "utf-8-sig") as temp:
        for (term, count) in zip(terms, counts):
            line = "('"+term[1]+"',"+str(term[2])+str(count)+"),\n"
            temp.write(line)
        temp.close()

all_news = get_all_news()
unique_words = get_unique_words()
in_docs = []
i=0
for term in unique_words:
    count = 0
    if i%100 ==0:
        print(i)
    for doc in all_news:
        if term[1]+' ' in doc[2]+' ':
            count = count + 1
    i = i + 1
    in_docs.append(count)
    # update_db(term[0], count)

write_to_file(unique_words, in_docs)