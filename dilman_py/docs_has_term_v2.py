# -*- coding: utf-8 -*-
import codecs

import mysql.connector

# including start and end
def get_all_news(start=-1, end=-1):
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()

    sql = 'SELECT * FROM tbl_all_news'
    if start!=-1 and end!=-1:
        sql = 'SELECT * FROM tbl_all_news where id>='+str(start)+' and id<='+str(end)
    mycursor.execute(sql)
    myresult = mycursor.fetchall()

    return myresult

# including start and end
def get_unique_words(start=-1, end=-1):
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()

    sql = 'SELECT * FROM news_word_freq_cleaned'
    if start!=-1 and end!=-1:
        sql = 'SELECT * FROM news_word_freq_cleaned where id>='+str(start)+' and id<='+str(end)

    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mydb.close()
    return myresult

def update_db(id, count):
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()
    sql = "UPDATE news_word_freq_cleaned SET docs = %s where id = %s"
    val = (count, id)
    mycursor.execute(sql, val)
    mydb.commit()

def write_to_file(terms, counts):
    with codecs.open("test_output.txt", "w", "utf-8-sig") as temp:
        for (term, count) in zip(terms, counts):
            line = "('"+term[1]+"',"+str(term[2])+str(count)+"),\n"
            temp.write(line)
        temp.close()

def update_db(id, count):
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()
    sql = "UPDATE news_word_freq_cleaned SET doc = %s where id = %s"
    val = (count, id)
    mycursor.execute(sql, val)
    mydb.commit()
# 227452
def start_do():
    all_news = get_all_news()
    for i in range(1,227):
        start = i*1000
        end = start+1000
        print('start='+str(start)+' , end='+str(end))
        all_unique_words = get_unique_words(start,end)
        in_docs = []
        i=0
        for term in all_unique_words:
            count = 0
            if i%100 ==0:
                print('Term '+str(i)+' is processed.')
            for doc in all_news:
                if ' '+term[1] in ' '+doc[2]:
                    count = count + 1
            i = i + 1
            # in_docs.append(count)
            update_db(term[0], count)

def start_do_tail():
    all_news = get_all_news()
    all_unique_words = get_unique_words(227000,227452)
    i=0
    for term in all_unique_words:
        count = 0
        if i%100 ==0:
            print('Term '+str(i)+' is processed.')
        for doc in all_news:
            if ' '+term[1] in ' '+doc[2]:
                count = count + 1
        i = i + 1
        update_db(term[0], count)

start_do()
# start_do_tail()
