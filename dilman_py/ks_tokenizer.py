class KurdishSoraniTokenizer:

    def __init__(self, sentence):
        self.sentence = sentence
        self.tokens = [token.strip() for token in sentence.split(' ')]
        self.index = 0
        self.len = len(self.tokens)

    def has_next_token(self):
        return self.index < self.len

    def next_token(self):
        next_token = None
        if self.index == 0:
            next_token = self.tokens[0]
        elif self.has_next_token():
            next_token = self.tokens[self.index]

        self.index += 1
        return next_token


# Usage
# obj = KurdishSoraniTokenizer(text)
# print(obj.tokens)
#
# token = obj.next_token()
#
# while(token is not None):
#     print(token)
#     print(obj.has_next_token())
#     token = obj.next_token()
