import operator
import warnings
from dilman_py.ks_tokenizer import KurdishSoraniTokenizer

from urllib.request import urlopen

class KurdishSoraniSpellCheker:
    unigram_dict = {}
    bigram_dict = {}

    num_of_valid_tokens = 62226455;
    num_of_valid_bigram_tokens = 62138192;

    # def inialize_dictioneries(self):
    #     print('Inializing Uni-Gram dictionar...')
    #     with open(self.unigram_dict_path, encoding='utf-8-sig', errors='ignore') as f:
    #         lines = f.readlines()
    #
    #     for line in lines:
    #         kv = line.split('\t')
    #         self.unigram_dict.setdefault(kv[0], int(kv[1].strip()))
    #     print('Done!')
    #
    #     print('Inializing Bi-Gram dictionar...')
    #     with open(self.bigram_dict_path, encoding='utf-8-sig', errors='ignore') as f:
    #         lines = f.readlines()
    #
    #     for line in lines:
    #         kv = line.split('\t')
    #         self.bigram_dict.setdefault(kv[0], int(kv[1].strip()))
    #     print('Done!')

    @classmethod
    def inialize_dictioneries(self):
        print('Inializing Uni-Gram dictionary...')
        unigram_dict_data = urlopen(
            'https://torbend.com/public/Dictionary.txt')  # it's a file like object and works just like a file

        for line in unigram_dict_data:
            line = line.decode('utf-8')
            kv = line.split('\t')
            self.unigram_dict.setdefault(kv[0], int(kv[1].strip()))
        print('Done!')

        print('Inializing Bi-Gram dictionary...')
        bigram_dict_data = urlopen(
            'https://torbend.com/public/BiGramDictionary.txt')  # it's a file like object and works just like a file

        for line in bigram_dict_data:

            line = line.decode('utf-8')
            kv = line.split('\t')
            self.bigram_dict.setdefault(kv[0], int(kv[1].strip()))
        print('Done!')

    @classmethod
    def is_valid(self, token):

        if token is None or len(token) is 0:
            warnings.warn('is_valid(self, token): Input token is not valid!')
            return False
        else:
            token = token.strip()
            if token not in self.unigram_dict:

                token = token.replace('،', '')
                token = token.replace('.', '')
                token = token.replace(':', '')

                if token not in self.unigram_dict:
                    return False
                else:
                    return True
    def detection(self, txt):
        misspelled_words = []
        if txt is None or len(txt) is 0:
            warnings.warn('detection(self, txt): Input text is not valid!')
            return None
        else:
            tokenizer = KurdishSoraniTokenizer(txt)
            token = tokenizer.next_token()

            while (token is not None):
                if not KurdishSoraniSpellCheker.is_valid(token):
                    misspelled_words.append(token)
                token = tokenizer.next_token()

        return misspelled_words


    # From http://norvig.com/spell-correct.html
    # For generating candidates
    def edits1(self, word):
        "All edits that are one edit away from `word`."
        # letters    = 'abcdefghijklmnopqrstuvwxyz'
        letters = 'قوەرڕتیێئحعۆپلڵکژهگغفدسشازخجچڤبنم'
        splits = [(word[:i], word[i:]) for i in range(len(word) + 1)]
        deletes = [L + R[1:] for L, R in splits if R]
        transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R) > 1]
        replaces = [L + c + R[1:] for L, R in splits if R for c in letters]
        inserts = [L + c + R for L, R in splits for c in letters]
        return set(deletes + transposes + replaces + inserts)

    # not used
    def edits2(self, word):
        "All edits that are two edits away from `word`."
        return (e2 for e1 in self.edits1(word) for e2 in self.edits1(e1))


    # calculate language model: P(c)
    # dict<string:candidate, double:probablity>
    def get_language_model(self, candidates):
        language_model = {}
        for candidate in candidates:
            probability = 0.1 / self.num_of_valid_tokens
            if candidate in self.unigram_dict:
                can_freq = self.unigram_dict[candidate]
                probability = can_freq / self.num_of_valid_tokens
            language_model.setdefault(candidate, probability)

        return language_model


    # calculate error model: P(w|c)
    def get_error_model(self, candidates, next_token):
        error_model = {}

        # nextToken must not be null
        # nextToken must be valid token (i.e. must exist in our dictionary)
        # candidate+nextToken must exist in te biagram dictionary
        # otherwise we cannot use it compute Error Model: P(w|c).
        # we use default value 1 in these cases

        bigram_token_prob = 0.1 / self.num_of_valid_bigram_tokens
        # 1 / num_of_valid_bigram_tokens will be very small and it will be the
        # same for all the candidates
        # we can also use any other constant for example '1',

        if KurdishSoraniSpellCheker.is_valid(next_token):
            for candidate in candidates:
                # Check whether each candidate+nextToken exist in our bigram_dict
                # If yes, then return its frequency/probability and use it to rank the candidate
                # If not, then use a very samll number to rank the candidate
                # (i.e. this candidate is not very important)
                bigram_token = candidate + ' ' + next_token

                if bigram_token in self.bigram_dict:
                    bigram_token_freq = self.bigram_dict[bigram_token]
                    bigram_token_prob = bigram_token_freq / self.num_of_valid_bigram_tokens

                error_model.setdefault(candidate, bigram_token_prob)
        else:
            error_model.fromkeys(candidates, 1)
            # error_model.fromkeys(candidates, bigram_token_prob)

        return error_model


    #  calculate P(c)*P(w|c)
    def get_combined_probability(self, language_model, error_model):
        combined_probability = {}
        combined_dict = [(k, language_model[k], v) for k, v in error_model.items()]

        for elem in combined_dict:
            candidate = elem[0]
            plm = elem[1]
            pem = elem[2]

            combined_probability.setdefault(candidate, plm * pem)
        return combined_probability

    def get_highest_candidate(self, token, next_token):
        all_candidates = self.edits1(token)
        candidates = []
        # remove those who are not valid (not in the dictionary)
        for candidate in all_candidates:
            if KurdishSoraniSpellCheker.is_valid(candidate):
                candidates.append(candidate)

        # If there are candidates for the given token, then compute their probabilities
        # Otherwise, return the misspelled token as it is
        if (len(candidates) > 0):
            language_model = self.get_language_model(candidates)
            error_model = self.get_error_model(candidates, next_token)
            result = self.get_combined_probability(language_model, error_model)

            # fine and return the entry that has the highest value
            if len(result) > 0:
                return max(result.items(), key=operator.itemgetter(1))[0]
            else:
                # print('TOKEN: ')
                # print(token)
                # print('NEXT_TOKEN: ')
                # print(next_token)
                # print('CANDIDATES: ')
                # print(candidates)
                # print('RESULT: ')
                # print(result)

                return token
        else:
            return token

    def correction(self, txt):
        corrected_txt = ''
        is_first = True
        if txt is None or len(txt) is 0:
            warnings.warn('correction(self, txt): Input text(txt='+txt+') is not valid!')
            return None
        else:
            tokenizer = KurdishSoraniTokenizer(txt)
            token = tokenizer.next_token()
            while (token is not None):
                # token ends or starts with dot or comma

                if KurdishSoraniSpellCheker.is_valid(token):
                    corrected_txt += token
                    token = tokenizer.next_token()
                else:
                    # If the token is not valid, then we need the next token, because
                    # our Error Model: P(w|c), is computed based on the probability of
                    # appearing each candidates for the token with the next token.
                    next_token = tokenizer.next_token()

                    # then, find the candidate which has the highest probability
                    # i.e. correct the misspelled token with the help of its neighbor
                    candidate = self.get_highest_candidate(token, next_token)
                    if candidate is None:
                        corrected_txt += token
                    else:
                        corrected_txt += candidate
                    token = next_token
                if is_first:
                    corrected_txt += ' '
            return corrected_txt

KurdishSoraniSpellCheker.inialize_dictioneries()




# Usage
# read a file from disk
# res_dir = "D:\\SpellChecking\\res\\"
# with open(res_dir + "test.txt", encoding='utf-8-sig', errors='ignore') as f:
#     lines = f.read()
#
# print('Input Text: ', lines)
# out = kssc.correction(lines)
# print('Cottected Text: ', out)

