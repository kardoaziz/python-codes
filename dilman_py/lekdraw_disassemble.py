import mysql.connector
from Word import Word

def get_words(type="s"):
  mydb = mysql.connector.connect(
    host="mysql.stackcp.com",
    user="nlpdbs-3137331da1",
    port="53940",
    password="nlpdbuser12345",
    database="nlpdbs-3137331da1"
  )
  mycursor = mydb.cursor()
  mycursor.execute("SELECT id, word, root, root2, root3, base, type_tepar "
                     "FROM vbgs where type='"+type+"'and deleted_at is null;")
  myresult = mycursor.fetchall()
  mydb.close()
  return myresult


def find_base_roots(s_words, d_words, l_words):

  word = []
  processed = 0
  for row in l_words:
    if row[1].endswith('کردن'):
      i = row[1].find('کردن')
      base  = row[1][0:i] + '_' + 'کرد' + '_'
      root1 = row[1][0:i] + '_' + 'کە' + '_'
      root2 = row[1][0:i] + '_' + 'کر' + '_'
      root3 = None
      word.append([row[0],row[1],base, root1, root2, root3])

    elif row[1].endswith('کردنەوە'):
      i = row[1].find('کردنەوە')
      base  = row[1][0:i] + '_'+'کرد'+'_'+'ەوە'
      root1 = row[1][0:i] + '_'+'کە'+'_'+'ەوە'
      root2 = row[1][0:i] + '_'+'کر'+'_'+'ەوە'
      root3 = None
      word.append([row[0],row[1],base, root1, root2, root3])
    elif row[1].endswith('بوون'):
      i = row[1].find('بوون')
      base = row[1][:-1] + '_'
      root1 = row[1][0:i] + '_' + 'ب' + '_'
      root2 = None
      root3 = None
      word.append([row[0], row[1], base, root1, root2, root3])

    elif row[1].endswith('بوونەوە'):
      i = row[1].find('بوونەوە')
      base = row[1][0:i] + 'بوو' + '_' + 'ەوە'
      root1 = row[1][0:i]+ '_' + 'ب' + '_' + 'ەوە'
      root2 = None
      root3 = None
      word.append([row[0], row[1], base, root1, root2, root3])
    elif row[1].endswith('گرتن'):
      i = row[1].find('گرتن')
      base = row[1][0:i] + '_' + 'گرت' + '_'
      root1 = row[1][0:i] + '_' + 'گر' + '_'
      root2 = row[1][0:i] + '_' + 'گیر' + '_'
      root3 = None
      word.append([row[0], row[1], base, root1, root2, root3])
    elif row[1].endswith('گرتنەوە'):
      i = row[1].find('گرتنەوە')
      base = row[1][0:i] + '_' + 'گرت' + '_' + 'ەوە'
      root1 = row[1][0:i]+ '_' + 'گر' + '_' + 'ەوە'
      root2 = row[1][0:i]+ '_' + 'گیر' + '_' + 'ەوە'
      root3 = None
      word.append([row[0], row[1], base, root1, root2, root3])
    elif row[1].endswith('دان'):
      i = row[1].find('دان')
      base  = row[1][0:i] + '_' + 'دا' + '_'
      root1 = row[1][0:i] + '_' + 'دە' + '_'
      root2 = row[1][0:i] + '_' + 'در' + '_'
      root3 = None
      word.append([row[0], row[1], base, root1, root2, root3])
    elif row[1].endswith('دانەوە'):
      i = row[1].find('دانەوە')
      base  = row[1][0:i] + '_' + 'دا' + '_' + 'ەوە'
      root1 = row[1][0:i] + '_' + 'دە' + '_' + 'ەوە'
      root2 = row[1][0:i] + '_' + 'در' + '_' + 'ەوە'
      root3 = None
      word.append([row[0], row[1], base, root1, root2, root3])
    elif row[1].endswith('هاتن'):
      i = row[1].find('هاتن')
      base = row[1][0:i] + 'هات' + '_'
      root1 = row[1][0:i] + '_' + 'ێ' + '_'
      root2 = row[1][0:i] + '_' + 'هێنر' + '_'
      root3 = row[1][0:i] + '_' + 'هێندر' + '_'
      word.append([row[0], row[1], base, root1, root2, root3])
    elif row[1].endswith('هاتنەوە'):
      i = row[1].find('هاتنەوە')
      base = row[1][0:i] + 'هات' + '_'+ 'ەوە'
      root1 = row[1][0:i] + '_' + 'ێ' + '_'+ 'ەوە'
      root2 = row[1][0:i] + '_' + 'هێنر' + '_'+ 'ەوە'
      root3 = row[1][0:i] + '_' + 'هێندر' + '_'+ 'ەوە'
      word.append([row[0], row[1], base, root1, root2, root3])

    elif row[1].endswith('کەوتن'):
      i = row[1].find('کەوتن')
      base  = row[1][0:i] + '_' + 'کەوت' + '_'
      root1 = row[1][0:i] + '_' + 'کەو' + '_'
      root2 = row[1][0:i] + '_' + 'کەوێنر' + '_'
      root3 =  row[1][0:i] + '_' + 'کەوێندر' + '_'
      word.append([row[0], row[1], base, root1, root2, root3])

    elif row[1].endswith('خستن'):
      i = row[1].find('خستن')
      base  = row[1][0:i] + '_' + 'خست' + '_'
      root1 = row[1][0:i] + '_' + 'خە' + '_'
      root2 = row[1][0:i] + '_' + 'خر' + '_'
      root3 = None
      word.append([row[0], row[1], base, root1, root2, root3])

    elif row[1].endswith('هێنان'):
      i = row[1].find('هێنان')
      base  = row[1][0:i] + '_' + 'هێنا' + '_'
      root1 = row[1][0:i] + '_' + 'هێنر' + '_'
      root2 = row[1][0:i] + '_' + 'هێندر' + '_'
      root3 = None
      word.append([row[0], row[1], base, root1, root2, root3])

    elif row[1].endswith('گەڕان'):
      i = row[1].find('گەڕان')
      base  = row[1][0:i] + '_' + 'گەڕا' + '_'
      root1 = row[1][0:i] + '_' + 'گەڕێ' + '_'
      root2 = row[1][0:i] + '_' + 'گەڕێنر' + '_'
      root3 = row[1][0:i] + '_' + 'گەڕێندر' + '_'
      word.append([row[0], row[1], base, root1, root2, root3])



  return word

# s_words[row][col]
s_words = get_words()
d_words = get_words("d")
l_words = get_words("l")

l_base_roots=find_base_roots(s_words, d_words, l_words)

def insert_to_db(l_base_roots):
  mydb = mysql.connector.connect(user='root', password='root',
                                 host='127.0.0.1',
                                 database='vbgs')
  mycursor = mydb.cursor()

  sql = "INSERT INTO lekdraw_base_roots (word_id, word, base, root1, root2, root3) VALUES (%s, %s,%s, %s,%s, %s)"
  for row in l_base_roots:
    val = (row[0], row[1], row[2], row[3], row[4], row[5])
    mycursor.execute(sql, val)
    mydb.commit()

# insert_to_db(l_base_roots)


def update_online_db(l_base_roots):
  mydb = mysql.connector.connect(
    host="mysql.stackcp.com",
    user="nlpdbs-3137331da1",
    port="53940",
    password="nlpdbuser12345",
    database="nlpdbs-3137331da1"
  )

  mycursor = mydb.cursor()

  for row in l_base_roots:
    sql = "UPDATE vbgs SET base=%s, root=%s, root2=%s, root3=%s WHERE id=%s"
    mycursor.execute(sql, (row[2], row[3],row[4], row[5], row[0]))
    mydb.commit()



def update_online_db_root23(l_base_roots):
  mydb = mysql.connector.connect(
    host="mysql.stackcp.com",
    user="nlpdbs-3137331da1",
    port="53940",
    password="nlpdbuser12345",
    database="nlpdbs-3137331da1"
  )

  mycursor = mydb.cursor()

  for row in l_base_roots:
    sql = "UPDATE vbgs SET root2=%s, root3=%s WHERE id=%s"
    mycursor.execute(sql, (row[4], row[5], row[0]))
    mydb.commit()


update_online_db(l_base_roots)

# change k to kr, tofiq suggested
# update_online_db_root23(l_base_roots)

for row in l_base_roots:
  print(row)

print(len(l_base_roots))