import pandas as pd
import mysql.connector
import math
import numpy as np


def get_all_news():
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()
    sql = 'SELECT * FROM tbl_all_news'
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mydb.close()

    return myresult

def get_unique_words():
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()

    sql = 'SELECT * FROM news_word_freq_cleaned'
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mydb.close()
    return myresult

def docs_has_term(all_docs, term):
    res = 0
    for doc in all_docs:
        if term+' ' in doc+' ':
            res = res + 1

    return res

def compute_tf(doc):
    bagOfWords = doc.split(' ')
    unique_words = set(bagOfWords)
    tfDict = {}
    bagOfWordsCount = len(bagOfWords)
    for word in unique_words:
        tfDict[word] = doc.count(word) / float(bagOfWordsCount)
    return tfDict


def compute_idf(all_docs, all_unique_words):
    idf_dic = {}
    n = len(all_docs)
    idf = np.zeros(n*len(all_unique_words),2)

    for word in all_unique_words:
        for doc in all_docs:
            num_docs_has_term = docs_has_term(all_docs, word[0])
            if num_docs_has_term != 0:
                idf [doc[0],word[0]] = math.log(n / float(num_docs_has_term))

    return idf

# all_news = get_all_news()
# all_unique_words = get_unique_words()

doc1=[1,'title1', 'the boy ate the apple.']
doc2=[2,'title2', 'the girl ate the banana.']


print('compute TF:')
df = compute_tf(doc1[2])

print(df)
print('Compute IDF:')

# for news in all_news:
#     tf = compute_tf(news)