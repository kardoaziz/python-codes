# coding=utf8
from gensim import models as mdl
import os.path
class MyUtils:
    list_of_places = []
    list_of_adjectives = []
    model = mdl.Word2Vec.load(os.path.dirname(os.path.abspath(__file__))+"/word2vec.model")

    with open(os.path.dirname(os.path.abspath(__file__))+'/places.txt', encoding='utf-8-sig', errors='ignore') as f:
        lines = f.readlines()
        for line in lines:
            list_of_places.append(line.strip())
        print('The list of places was inialized...')

    with open(os.path.dirname(os.path.abspath(__file__))+'/adjectives.txt', encoding='utf-8-sig', errors='ignore') as f:
        lines = f.readlines()
        for line in lines:
             list_of_adjectives.append(line.strip())
        print('The list of adjectives was inialized...')

    model = mdl.Word2Vec.load(os.path.dirname(os.path.abspath(__file__))+"/word2vec.model")

    @classmethod
    def is_place(self, token = ''):
        # if token is None or len(token) < 3:
        #     print('is_place('+token+'): The given token is not valid!')
        #     return False
        # else:
        for place in self.list_of_places:
            if place == token:
                return True
        return False

    @classmethod
    def is_adj(self, token = ''):
        # if token is None or len(token) < 3:
        #     print('is_place('+token+'): The given token is not valid!')
        #     return False
        # else:
        for place in self.list_of_adjectives:
            if place == token:
                return True
        return False

    @classmethod
    def get_similar_tokens(self, token, n=5):

        if token in self.model.wv:
            vector = self.model.wv[token]       # get numpy vector of a word
            #        print(len(vector))
            sims = self.model.wv.most_similar(vector, topn=n)  # get other similar words

            return  [item[0] for item in sims]
        else:
            return [token, token, token, token, token]


# Usage
# outside this class:
# from IsA import IsA
# print(IsA.is_place(token))
# print(sentence)
# for token in sentence.split(' '):
#     if MyUtils.is_place(token):
#         print(token + ' is place.')
#     if MyUtils.is_adj(token):
#         print(token + 'is ADJ')
#
# print('----------------------')
#
# print('----------------------')


# bb = MyUtils.get_similar_tokens('تێکچوون')
# print(bb)