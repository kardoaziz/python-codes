import csv
from urllib.request import urlopen

import mysql.connector
from klpt.preprocess import Preprocess

from dilman_py.ksscs import KurdishSoraniSpellCheker

def insert_into_online_db(words):
    mydb = mysql.connector.connect(
        host="62.201.215.205",
        user="testuser",
        password="nlp!3£$%5ADm",
        database="nlp"
    )
    mycursor = mydb.cursor()

    for word in words:
        sql = "INSERT INTO stop_words (word) VALUES ('"+word+"')"
        mycursor.execute(sql)
        mydb.commit()


# with open('D:\Torbend\Kurdish Stop Words.csv', newline='', encoding='utf-8') as csvfile:
#     reader = csv.DictReader(csvfile)
#     stop_words = []
#     for row in reader:
#         stop_words.append(row['word'])
#
# insert_into_online_db(stop_words)

# dir_path = 'AsoSoftBigFiles\\AsoSoftBig_'
# file_index = 0
# with open('AsoSoftBig.txt', encoding="utf8", errors='ignore') as f:
#     lines = f.readlines()
#     for line in lines:
#         file_index += 1
#         path = dir_path+str(file_index)+'.txt'
#         with open(path, 'a', encoding='utf-8') as f:
#             f.write(line)


# open adjective.txt and convert its content to unicode
# then save the result in adjectives.txt

def get_adjectives(file_path):
    adj = []
    preprocessor_ckb = Preprocess("Sorani", "Arabic", numeral="Latin")

    with open(file_path, mode='r', encoding="utf-16") as f:
        lines = f.readlines()
        for line in lines:
            adj.append(preprocessor_ckb.normalize(line).strip())
    return adj

def save_adj_to_file(file_name, adjs):
    with open(file_name, mode="a", encoding="utf-8-sig") as file:
        for adj in adjs:
            file.write(adj)
            file.write('\n')

        file.close()


# adjs = get_adjectives('adjective.txt')
# print(adjs)
# save_adj_to_file('adjectives.txt', adjs)


print('Inializing Uni-Gram dictionar...')
unigram_dict_data = urlopen(
        'https://torbend.com/public/Dictionary.txt')  # it's a file like object and works just like a file

for line in unigram_dict_data:
    line = line.decode('utf-8')
    kv = line.split('\t')

print('Done!')


from dilman_py.ksscs import KurdishSoraniSpellCheker

txt = 'حکمەتی هەریمی کوردستان بڕیایری ئازادکردنی چەند بەدنکراوێکی هەڵوەشاندەوە. ئەو بڕیایرە سالی پارلەلایەن جێگەی سەرۆک وەزیرانەوە دەرکرابوو.'
print('Original Text: ', txt)

kssc = KurdishSoraniSpellCheker()

list_of_misspelled_words = kssc.detection(txt)
print('Detected misspelled words:',list_of_misspelled_words)

corrected_txt = kssc.correction(txt)
print('Corrected Text: ', corrected_txt)



