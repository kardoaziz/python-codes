from tabulate import tabulate
import numpy

res_dir      = "D:\\SpellChecking\\res\\"
unigram_dict_path = res_dir+"Dictionary.txt"
bigram_dict_path  = res_dir+"BiGramDictionary.txt"
unigram_dict = {}

def inialize_dictioneries():
    with open(unigram_dict_path, encoding='utf-8-sig', errors='ignore') as f:
        lines = f.readlines()

        for line in lines:
            kv = line.split('\t')
            unigram_dict.setdefault(kv[0], int(kv[1].strip()))





inialize_dictioneries()



counts = numpy.zeros(20, int)
print(counts)
for key in unigram_dict:
    if unigram_dict[key] < 20:
        counts[unigram_dict[key]]+=1
print(counts)
print('================================')
print('\n')

tabular_list = []
for i in range(20):
    tabular_list.append([i, counts[i]])

tabular_list.append(['[Sum(0-19)', sum(counts)])

print(tabulate(tabular_list, headers=['freq.', 'num. of words'], tablefmt='orgtbl'))
