def get_adjectives(file_path, deli=' '):
    tokens = []
    file_tokens_dict = {}
    with open(file_path, encoding='utf-8-sig', errors='ignore') as f:
        lines = f.readlines()
        # print(len(lines))
        for line in lines:
            tokens.append(line.split(deli))
    return tokens


def flatten_list(input):
    new_list = []
    for i in input:
        for j in i:
            new_list.append(j)
    return new_list

def from_friles_to_one_file():
    delimiters = ['. ', '، ', '، ', '، ', '، ', '، ']
    all_adjectives = []
    dir_path = 'D:\\Torbend\\DataSet\\Dr.AbdulJabar\\Adjective_AwalNaw\\'
    for i in range(6):
        i += 1
        res = get_adjectives(dir_path + str(i) + '.txt', delimiters[i - 1])

        for i in res:
            for j in i:
                all_adjectives.append(j)

    with open(dir_path + 'all_adjs.txt', mode="a", encoding="utf-8-sig") as all_adj_file:
        for adj in all_adjectives:
            adj = ((adj.replace('.', '')).replace('،', '')).strip()
            all_adj_file.write(f"{adj}\n")
            print(adj)
        all_adj_file.close()

    all_adjectives_unique = set(all_adjectives)
    print(len(all_adjectives))
    print(len(all_adjectives_unique))

    with open(dir_path + 'all_unique_adjs.txt', mode="a", encoding="utf-8-sig") as all_adj_file:
        for adj in all_adjectives_unique:
            adj = ((adj.replace('.', '')).replace('،', '')).strip()
            all_adj_file.write(f"{adj}\n")
            print(adj)
        all_adj_file.close()