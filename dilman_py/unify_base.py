def get_bases(file_path, deli=' '):
    tokens = []
    file_tokens_dict = {}
    with open(file_path, encoding='utf-8-sig', errors='ignore') as f:
        lines = f.readlines()
        # print(len(lines))
        for line in lines:
            tokens.append(line.split(deli))
    return tokens


def flatten_list(input):
    new_list = []
    for i in input:
        for j in i:
            new_list.append(j)
    return new_list

delimiters = ['،','،','،']
all_bases = []
dir_path = 'D:\\Torbend\\DataSet\\Dr.AbdulJabar\\Chawg\\base_'
for i in range(3):
    i+=1
    res = get_bases(dir_path+str(i)+'.txt', delimiters[i-1])

    for i in res:
        for j in i:
            all_bases.append(j)

with open(dir_path+'all_bases.txt', mode="a", encoding="utf-8-sig") as all_base_file:
    for base in all_bases:
        base=((base.replace('.','')).replace('،','')).strip()
        all_base_file.write(f"{base}\n")
        print(base)
    all_base_file.close()


all_basess_unique = set(all_bases)
print(len(all_bases))
print(len(all_basess_unique))

with open(dir_path+'all_unique_basess.txt', mode="a", encoding="utf-8-sig") as all_base_file:
    for base in all_basess_unique:
        base=((base.replace('.','')).replace('،','')).strip()
        all_base_file.write(f"{base}\n")
    all_base_file.close()