
import generate_sada_tofiq
import generate_darezhraw_tofiq
import generate_lekdraw_tofiq
import generate_ranbrdu



# ڕابردووی سادە
# generate_sada_tofiq.generate_rabrdooy_bardawam("pn"," and id not in (select distinct id_vbgs from generated_words where id_vbgs=vbgs.id and tense='ڕابردووی بەردەوام')",1)
# generate_sada_tofiq.generate_rabrdooy_door("pn"," and id not in (select distinct id_vbgs from generated_words where id_vbgs=vbgs.id and tense='ڕابردووی دوور')",1)
# generate_sada_tofiq.generate_wistw_margi("pn"," and id not in (select distinct id_vbgs from generated_words where id_vbgs=vbgs.id and tense='ڕابردووی ویستوئارەزوو و مەرجیی')",1)
#
#  ڕابردوی داڕێژراو
# generate_darezhraw_tofiq.generate_rabrdooy_tawaw("pn", " and id not in (select distinct id_vbgs from generated_words where id_vbgs=vbgs.id and tense='ڕابردووی تەواو')", 1)
# generate_darezhraw_tofiq.generate_rabrdooy_tawaw_danani("pn", " and id not in (select distinct id_vbgs from generated_words where id_vbgs=vbgs.id and tense='ڕابردووی تەواو دانانی')", 1)
# generate_darezhraw_tofiq.generate_wistiyu_marji("pn", " and id not in (select distinct id_vbgs from generated_words where id_vbgs=vbgs.id and tense='ویستی و مەرجی')", 1)
#
# # ڕابردووی لێکدراو100529
generate_lekdraw_tofiq.generate_rabrdooy_tawaw("pn", " and id not in (select distinct id_vbgs from generated_words where id_vbgs=vbgs.id and tense='ڕابردووی تەواو')", 1)
generate_lekdraw_tofiq.generate_rabrdooy_tawaw_danani("pn", " and id not in (select distinct id_vbgs from generated_words where id_vbgs=vbgs.id and tense='ڕابردووی تەواوی دانانی')", 1)
generate_lekdraw_tofiq.generate_wistiyu_marji("pn", " and id not in (select distinct id_vbgs from generated_words where id_vbgs=vbgs.id and tense='ویستی و مەرجی')", 1)
#
# # ڕانەبردووی سادە
generate_ranbrdu.generate_ranabrdooy_sada("pn", " and id not in (select distinct id_vbgs from generated_words where id_vbgs=vbgs.id and tense='ڕانەبردووی سادە')", 1)
generate_ranbrdu.generate_ranabrdooy_danani_sada("pn", " and id not in (select distinct id_vbgs from generated_words where id_vbgs=vbgs.id and tense='ڕانەبردووی دانانی')", 1)