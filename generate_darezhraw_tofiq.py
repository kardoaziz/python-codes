import json
import re
from Word import Word
import generate_sada_tofiq as gs
from mysql.connector.constants import ClientFlag
import mysql.connector


# mydb = mysql.connector.connect(
#   host="62.201.215.205",
#   user="testuser",
#   password="nlp!3£$%5ADm",
#   database="nlp"
# )
mydb = mysql.connector.connect(
    host="mysql.gb.stackcp.com",
    user="torbend-3231318204",
    password="vxwhyj8shy",
    database="torbend-3231318204",
    port="59054"
)

# mydb = mysql.connector.connect(
#   host="mysql.stackcp.com",
#   user="nlpdbs-3137331da1",
#   port="53940",
#   password="nlpdbuser12345",
# database="nlpdbs-3137331da1"
# )
all_words = []


def getallgeneratedwords():
    mycursor = mydb.cursor()
    mycursor.execute("select * from generated_words  ")
    result = mycursor.fetchall()  # fetches all the rows in a result set
    for i in result:
        all_words.append(i[1])
    print(len(all_words))



def generate_rabrdooy_tawaw(status="pn",sql="",saveit=0):
    getallgeneratedwords()
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='d' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1;
    words = []
    aspect1 = "و"
    aspect2 = "وو"
    aspect3 = "ی"
    p_aspect1 = "ەت"
    p_aspect2 = "ە"
    neg_prefix = "نە"
    pro=""
    print("ڕابردووی تەواو")
    for i in result:

        word = Word(i[1], i[3], i[6], i[7], i[8],_id_vbgs=i[0])
        neg_tenses = []

        # print(x, ":", word.word, ":", i[0])
        # print(word.base)
        # print(word.root)
        # print(word.type_tepar)

        # print(word)
        # print("ڕابردووی تەواو")
        tenses = []

        pronoun = gs.getPronounn(word.type_tepar)
        barkars = gs.getPronounn("tn")
        bar_kar = ""

        for j2 in barkars:
            for j in pronoun:
                g_word=""
                if status == "p" or status == "pn": # are
                    if word.type_tepar == "t":  # tepar
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        if (word.base.count("_") == 1):  # 1 underscore peshgr nye
                            char_befor_underscore=word.base[word.base.index("_")-1]
                            pashgr=word.base[word.base.index("_")+1:]
                            if char_befor_underscore=="ا" or char_befor_underscore=="ی":
                                g_word = word.replace(word.base, '_', aspect1 + pronoun[j]+bbk  +p_aspect1, 0)
                            else:
                                if pashgr!="" and pashgr=="ەوە":
                                    g_word=word.replace(word.base, '_', aspect2 + pronoun[j]+bbk  +p_aspect1, 0)
                                else:
                                    g_word = word.replace(word.base, '_', aspect2 + pronoun[j]+bbk + p_aspect2, 0)
                        else:  # peshgr haya
                            underscore_index = [m.start() for m in re.finditer("_", word.base)]
                            char_befor_underscore = word.base[underscore_index[-1]-1]
                            pashgr = word.base.endswith("_")
                            if pashgr: # pashgr nabet
                                if j2 == "3t":
                                    bbk = "ە"
                                if char_befor_underscore=="ا" or char_befor_underscore=="ی":
                                    g_word = word.replace(word.base, '_', pronoun[j] , 0)
                                    g_word = word.replace(g_word, '_', aspect1+bbk, -1)
                                elif char_befor_underscore=="د" or char_befor_underscore=="ت":
                                    g_word = word.replace(word.base, '_', pronoun[j], 0)
                                    g_word = word.replace(g_word, '_', aspect2 +bbk, -1)
                                elif char_befor_underscore=="و":
                                    g_word = word.replace(word.base, '_', pronoun[j], 0)
                                    g_word = word.replace(g_word, '_', bbk, -1)
                            else: #pashgr habet
                                g_word = word.replace(word.base, '_', pronoun[j], 0)
                                if char_befor_underscore=="ا" or char_befor_underscore=="ی":
                                    g_word = word.replace(g_word, '_', aspect1+bbk+p_aspect1, -1)
                    else: # tenapar
                        if (word.base.count("_") == 1):  # 1 underscore peshgr nye
                            char_befor_underscore=word.base[word.base.index("_")-1]
                            pashgr=word.base[word.base.index("_")+1:]
                            if pashgr!="":
                                pro=pronoun[j]
                                # if j == "3t":
                                #     pro="ە"
                                g_word = word.replace(word.base, '_', aspect1 + pro  +p_aspect1, 0)
                            else:
                                 g_word = word.replace(word.base, '_', aspect1 + pronoun[j] , 0)
                        else:  # peshgr haya
                            underscore_index = [m.start() for m in re.finditer("_", word.base)]
                            char_befor_underscore = word.base[underscore_index[-1]-1]
                            pashgr = word.base.endswith("_")
                            if pashgr: # pashgr nabet
                                pro = pronoun[j]
                                if j == "3t":
                                    pro = "ە"
                                if char_befor_underscore=="ا" or char_befor_underscore=="ی":
                                    g_word = word.replace(word.base, '_', aspect1+pro, -1)
                                elif char_befor_underscore=="د" or char_befor_underscore=="ت":
                                    g_word = word.replace(word.base, '_', aspect2+pro, -1)
                                elif char_befor_underscore=="و":
                                    g_word = word.replace(word.base, '_', pro, -1)
                            else: #pashgr habet
                                if char_befor_underscore=="و":
                                    g_word = word.replace(word.base, '_', pronoun[j] + p_aspect1, -1)
                                else:
                                    g_word = word.replace(word.base, '_', aspect1 + pronoun[j] + p_aspect1, -1)
                    if g_word != "" and g_word not in all_words:
                        ww = word.cleanit(g_word)
                        if not [item for item in tenses if ww in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            tenses.append((ww, j, bar_kar))
                if status == "n" or status == "pn": # nareni
                    if word.type_tepar == "t":  # tepar
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        if word.base.count("_") == 1:  # 1 underscore peshgr nye
                            char_befor_underscore=word.base[word.base.index("_")-1]
                            pashgr=word.base[word.base.index("_")+1:]
                            if char_befor_underscore=="ا" or char_befor_underscore=="ی":
                                g_word =neg_prefix+ pronoun[j] + word.replace(word.base, '_',aspect1+bbk + p_aspect1, 0)
                            else:
                                if pashgr!="" and pashgr=="ەوە":
                                    g_word=neg_prefix+ pronoun[j] +word.replace(word.base, '_', aspect2+bbk + p_aspect1, 0)
                                else:
                                    g_word = neg_prefix+ pronoun[j] +word.replace(word.base, '_', aspect2+bbk + p_aspect2, 0)
                        else:  # peshgr haya
                            underscore_index = [m.start() for m in re.finditer("_", word.base)]
                            char_befor_underscore = word.base[underscore_index[-1]-1]
                            pashgr = word.base.endswith("_")
                            if word.base.count("_") == 2:
                                nu=2
                            elif word.base.count("_") == 3:
                                nu=3
                            if pashgr: # pashgr nabet
                                if j2 == "3t":
                                    bbk = "ە"
                                if char_befor_underscore=="ا" or char_befor_underscore=="ی":
                                    if nu==2:
                                        g_word = word.replace(word.base, '_', pronoun[j]+neg_prefix , 0)
                                    elif nu==3:
                                        g_word = word.replace(word.base, '_', neg_prefix , -2)
                                        g_word = word.replace(g_word, '_',  pronoun[j], 0)
                                    g_word = word.replace(g_word, '_', aspect1+bbk, -1)
                                elif char_befor_underscore=="د" or char_befor_underscore=="ت":
                                    if nu==2:
                                        g_word = word.replace(word.base, '_', pronoun[j]+neg_prefix , 0)
                                    elif nu==3:
                                        g_word = word.replace(word.base, '_', neg_prefix , -2)
                                        g_word = word.replace(g_word, '_',  pronoun[j], 0)
                                    g_word = word.replace(g_word, '_', aspect2 + bbk, -1)
                                elif char_befor_underscore=="و":
                                    if nu==2:
                                        g_word = word.replace(word.base, '_', pronoun[j]+neg_prefix , 0)
                                    elif nu==3:
                                        g_word = word.replace(word.base, '_', neg_prefix , -2)
                                        g_word = word.replace(g_word, '_',  pronoun[j], 0)
                                    g_word = word.replace(g_word, '_', bbk, -1)
                            else: #pashgr habet
                                if nu == 2:
                                    g_word = word.replace(word.base, '_', pronoun[j]+neg_prefix , 0)
                                elif nu == 3:
                                    g_word = word.replace(word.base, '_', neg_prefix, -2)
                                    g_word = word.replace(g_word, '_', pronoun[j], 0)
                                if char_befor_underscore=="ا" or char_befor_underscore=="ی":
                                    g_word = word.replace(g_word, '_', aspect3+bbk, -1)
                    else: # tenapar nare
                        if (word.base.count("_") == 1):  # 1 underscore peshgr nye
                            char_befor_underscore=word.base[word.base.index("_")-1]
                            pashgr=word.base[word.base.index("_")+1:]
                            if pashgr!="":
                                pro=pronoun[j]
                                # if j == "3t" :
                                #     pro="ە"
                                g_word = neg_prefix + word.replace(word.base, '_', aspect1 + pro  +p_aspect1, 0)
                            else:
                                 g_word = neg_prefix + word.replace(word.base, '_',  aspect1 + pronoun[j] , 0)
                        else:  # pashgr haya
                            underscore_index = [m.start() for m in re.finditer("_", word.base)]
                            char_befor_underscore = word.base[underscore_index[-1]-1]
                            pashgr = word.base.endswith("_")
                            if pashgr: # pashgr nabet
                                pro = pronoun[j]
                                g_word = word.replace(word.base, '_', neg_prefix, -2)
                                if j == "3t":
                                    pro = "ە"
                                if char_befor_underscore=="ا" or char_befor_underscore=="ی":
                                    g_word = word.replace(g_word, '_', aspect1+pro, -1)
                                elif char_befor_underscore=="د" or char_befor_underscore=="ت":
                                    g_word = word.replace(g_word, '_', aspect2+pro, -1)
                                elif char_befor_underscore=="و":
                                    g_word = word.replace(g_word, '_', pro, -1)
                            else: #pashgr habet
                                g_word = word.replace(word.base, '_', neg_prefix, -2)
                                if char_befor_underscore=="و":
                                    g_word = word.replace(g_word, '_', pronoun[j] + p_aspect1, -1)
                                else:
                                    g_word = word.replace(g_word, '_', aspect1 + pronoun[j] + p_aspect1, -1)
                    if g_word != "" and g_word not in all_words:
                        ww = word.cleanit(g_word)
                        if not [item for item in neg_tenses if ww in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            neg_tenses.append((ww, j, bar_kar))
        if saveit == 1:
            word.saveword(tenses, "ڕابردووی تەواو", 0,"داڕێژراو")
            word.saveword(neg_tenses, "ڕابردووی تەواو", 1,"داڕێژراو")
        print(tenses)
        print(neg_tenses)

# generate_rabrdooy_tawaw('n')

def generate_rabrdooy_tawaw_danani(status="pn",sql="",saveit=0):
    getallgeneratedwords()
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='d' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1;
    words = []
    print("ڕابردووی تەواو دانانی")
    for i in result:

        word = Word(i[1], i[3], i[6], i[7], i[8], _id_vbgs=i[0])
        neg_tenses = []

        # print(x, ":", word.word, ":", i[0])
        # print(word.base)
        # print(word.root)
        # print(word.type_tepar)

        # print(word)
        # print("ڕابردووی تەواو دانانی")
        tenses = []
        aspect1 = "بێت"
        aspect2="وو"
        aspect3="ی"
        p_aspect1="ەت"
        p_aspect2="ە"
        neg_prefix = "نە"
        pronoun = gs.getPronounn(word.type_tepar)
        barkars = gs.getPronounn("tn")

        for j2 in barkars:
            for j in pronoun:
                g_word=""
                if status == "p" or status == "pn": # are
                    if word.type_tepar == "t":  # tepar
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        if (word.base.count("_") == 1):  # 1 underscore peshgr nye
                            g_word = word.replace(word.base, '_', aspect1 + pronoun[j]+bbk , -1)
                        else:  # pashgr haya
                            g_word = word.replace(word.base, '_', pronoun[j], 0)
                            g_word = word.replace(g_word, '_', aspect1+bbk, -1)
                    else: # tenapar
                        g_word = word.replace(word.base, '_', aspect1 + pronoun[j] , -1)
                    if g_word!="" and g_word not in all_words:
                        ww = word.cleanit(g_word)
                        if not [item for item in tenses if ww in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            tenses.append((ww, j, bar_kar))
                if status == "n" or status == "pn": # nareni
                    if word.type_tepar == "t":  # tepar
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        if word.base.count("_") == 1:  # 1 underscore peshgr nye
                            g_word = neg_prefix + pronoun[j] + word.replace(word.base, '_', aspect1+bbk  , -1)
                        else:  # pashgr haya
                            if word.base.count("_") == 2:
                                g_word = word.replace(word.base, '_', pronoun[j] + neg_prefix, 0)
                            elif word.base.count("_") == 3:
                                g_word = word.replace(word.base, '_', neg_prefix, -2)
                                g_word = word.replace(g_word, '_', pronoun[j], 0)
                            g_word = word.replace(g_word, '_', aspect1+bbk, -1)
                    else: # tenapar
                        if word.base.count("_") == 1:  # 1 underscore peshgr nye
                            g_word =neg_prefix + word.replace(word.base, '_', aspect1 + pronoun[j] , -1)
                        else:
                            g_word =  word.replace(word.base, '_', neg_prefix , -2)
                            g_word = word.replace(g_word, '_', aspect1 + pronoun[j], -1)
                    if g_word!="" and g_word not in all_words:
                        ww = word.cleanit(g_word)
                        if not [item for item in neg_tenses if ww in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            neg_tenses.append((ww, j, bar_kar))
        if saveit == 1:
            word.saveword(tenses, "ڕابردووی تەواو دانانی", 0, "داڕێژراو")
            word.saveword(neg_tenses, "ڕابردووی تەواو دانانی", 1, "داڕێژراو")
        print(tenses)
        print(neg_tenses)



def generate_wistiyu_marji(status="pn",sql="",saveit=0):
    getallgeneratedwords()
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='d' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1;
    words = []
    print("ویستی و مەرجی")
    for i in result:

        word = Word(i[1], i[3], i[6], i[7], i[8], _id_vbgs=i[0])
        neg_tenses = []

        # print(x, ":", word.word, ":", i[0])
        # print(word.base)
        # print(word.root)
        # print(word.type_tepar)

        # print(word)
        # print("ویستی و مەرجی ")
        tenses = []
        aspect1 = "ب"
        aspect2="وو"
        aspect3="ی"
        p_aspect1="ەت"
        p_aspect2="ە"
        neg_prefix = "نە"
        pronoun = gs.getPronounn(word.type_tepar)

        for j in pronoun:
            g_word=""
            if status == "p" or status == "pn": # are
                g_word = word.replace(word.base, '_', aspect1 + pronoun[j] , -1)
                if g_word != "" and g_word not in all_words:
                    ww = word.cleanit(g_word)
                    if not [item for item in tenses if ww in item]:
                        tenses.append((ww, j, ''))
            if status == "n" or status == "pn": # nareni
                if word.type_tepar == "t":  # tepar
                    if word.base.count("_") == 1:  # 1 underscore peshgr nye
                        g_word = neg_prefix  + word.replace(word.base, '_', aspect1 + pronoun[j] , -1)
                    else:  # pashgr haya
                        if word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', neg_prefix, 0)
                        elif word.base.count("_") == 3:
                            g_word = word.replace(word.base, '_', neg_prefix, -2)
                            # g_word = word.replace(g_word, '_', pronoun[j], 0)
                        g_word = word.replace(g_word, '_', aspect1+pronoun[j] , -1)
                else: # tenapar
                    if word.base.count("_") == 1:  # 1 underscore peshgr nye
                        g_word =neg_prefix + word.replace(word.base, '_', aspect1 + pronoun[j] , -1)
                    else:
                        g_word =  word.replace(word.base, '_', neg_prefix , -2)
                        g_word = word.replace(g_word, '_', aspect1 + pronoun[j], -1)
                if g_word != "" and g_word not in all_words:
                    ww = word.cleanit(g_word)
                    if not [item for item in neg_tenses if ww in item]:
                        neg_tenses.append((ww, j, ''))


        if saveit == 1:
            word.saveword(tenses, "ویستی و مەرجی", 0, "داڕێژراو")
            word.saveword(neg_tenses, "ویستی و مەرجی", 1, "داڕێژراو")
        print(neg_tenses)
        print(neg_tenses)

# generate_rabrdooy_tawaw()
# generate_rabrdooy_tawaw_danani()
# generate_wistiyu_marji()