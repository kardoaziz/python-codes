import mysql.connector
from Word import Word
import generate_sada_tofiq

mydb = mysql.connector.connect(
    host="mysql.gb.stackcp.com",
    user="torbend-3231318204",
    password="vxwhyj8shy",
    database="torbend-3231318204",
    port="59054"
)


all_words = []


def getallgeneratedwords():
    mycursor = mydb.cursor()
    mycursor.execute("select * from generated_words  ")
    result = mycursor.fetchall()  # fetches all the rows in a result set
    for i in result:
        all_words.append(i[1])
    print(len(all_words))


def rabrdui_sada(negative="np", sql="",save=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='l' and deleted_at is null  "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix = "نە"
    k =1
    for i in result:
        print(k)
        k+=1
        # print(i[1])
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0])
        print(word.word)
        # print(word.type_tepar)
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn(
            "t")
        barkars = generate_sada_tofiq.getPronounn("tn")
        # if word.type_tepar == "t":
        #     barkars =  {**barkars, '3k': ''}
        # print(barkars)
        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        g_word = word.replace(word.base, '_', pronouns[j], 0)
                        if j2[0] != j[0] :
                            bbk = barkars[j2]
                        else:
                            if j2[0] != '3':
                                continue
                        if i[13] == 1:
                            g_word = word.replace(g_word, '_', bbk, -1)
                        else:
                            bbk = ''
                            g_word = word.replace(g_word, '_', bbk, -1)
                    else:
                        g_word = word.replace(word.base, '_', pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))
        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            if j2[0] != '3':
                                # print(j2[0])
                                # print(pronouns[j] + ":" + barkars[j2] + " skipped")
                                continue
                        if word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', pronouns[j] + prefix, 0)
                        else:
                            g_word = word.replace(word.base, '_', pronouns[j], 0)
                            g_word = word.replace(g_word, '_', prefix, -2)
                        if i[13] == 1:
                            g_word = word.replace(g_word, '_', bbk, -1)
                        else:
                            bbk = ''
                            g_word = word.replace(g_word, '_', bbk, -1)
                    else:
                        if word.base.count("_") == 1:
                            g_word = prefix + word.replace(word.base, '_', pronouns[j], -1)
                        else:
                            g_word = word.replace(word.base, '_', prefix, -2)
                            g_word = word.replace(g_word, '_', pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save==1:
            word.saveword(tenses, "ڕابردووی سادە", 0, "لێکدراو")
            word.saveword(neg_tenses, "ڕابردووی سادە", 1, "لێکدراو")


def rabrdui_bardawam(negative="np",sql="",save=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='l' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix = "دە"
    prefix2 = "نە"
    k =1
    for i in result:
        print(k)
        k+=1
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0])
        print(word.base)
        # print(word.type_tepar)
        # print(word.cleanit(word.base))
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn(
            "t")
        barkars = generate_sada_tofiq.getPronounn("tn")

        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        bbk = ""
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            if j2[0] != '3':
                                continue
                        if word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', pronouns[j] + prefix, 0)
                        else:
                            g_word = word.replace(word.base, '_', pronouns[j], 0)
                            g_word = word.replace(g_word, '_', prefix, -2)
                        if i[13] != 1:
                            bbk = ''
                        g_word = word.replace(g_word, '_', bbk, -1)

                    else:
                        if word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', prefix, 0)
                            g_word = word.replace(g_word, "_", pronouns[j], -1)
                        else:
                            g_word = word.replace(word.base, '_', pronouns[j], -1)
                            if i[13] == 1:
                                g_word = word.replace(g_word, '_', pronouns[j] + bbk, -1)
                            else:
                                g_word = word.replace(g_word, '_', pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))
        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            if j2[0] != '3':
                                continue
                        if word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', pronouns[j] + prefix2 + prefix, 0)
                        else:
                            g_word = word.replace(word.base, '_', pronouns[j], 0)
                            g_word = word.replace(g_word, '_', prefix2 + prefix, -2)
                        if i[13] != 1:
                            bbk = ''
                        g_word = word.replace(g_word, '_', bbk, -1)
                    else:
                        g_word = word.replace(word.base, '_', prefix2 + prefix, -2)
                        g_word = word.replace(g_word, '_', pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save==1:
            word.saveword(tenses, "ڕابردووی بەردەوام", 0, "لێکدراو")
            word.saveword(neg_tenses, "ڕابردووی بەردەوام", 1, "لێکدراو")


def rabrdui_dur(negative="np",sql="",save=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='l' and deleted_at is null  "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix = "بوو"
    prefix2 = "نە"
    k =1
    for i in result:
        print(k)
        k+=1
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0])
        print(word.base)
        # print(word.type_tepar)
        # print(word.cleanit(word.base))

        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn(
            "t")
        barkars = generate_sada_tofiq.getPronounn("tn")
        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            if j2[0] != '3':
                                continue
                        g_word = word.replace(word.base, '_', pronouns[j], 0)
                        # g_word = word.replace(g_word, '_', prefix, -1)
                        if i[13] == 1:
                            g_word = word.replace(g_word, '_', prefix + bbk, -1)
                        else:
                            g_word = word.replace(g_word, '_', prefix, -1)
                    else:
                        g_word = word.replace(word.base, "_", prefix + pronouns[j], -1)

                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))
        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            if j2[0] != '3':
                                continue
                        if word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', pronouns[j] + prefix2, 0)
                            if i[13] == 1:
                                g_word = word.replace(g_word, '_', prefix + bbk, -1)
                            else:
                                g_word = word.replace(g_word, '_', prefix, -1)
                        else:
                            g_word = word.replace(word.base, '_', pronouns[j], 0)
                            g_word = word.replace(g_word, '_', prefix2, -2)
                            if i[13] == 1:
                                g_word = word.replace(g_word, '_', prefix + bbk, -1)
                            else:
                                g_word = word.replace(g_word, '_', prefix, -1)
                    else:
                        if word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', prefix2, 0)
                            g_word = word.replace(g_word, '_', prefix + pronouns[j], -1)
                        else:
                            g_word = word.replace(word.base, '_', prefix2, -2)
                            g_word = word.replace(g_word, '_', prefix + pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save==1:
            word.saveword(tenses, "ڕابردووی دوور", 0, "لێکدراو")
            word.saveword(neg_tenses, "ڕابردووی دوور", 1, "لێکدراو")


def rabrdui_nzik_danani(negative="np",sql="",save=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='l' and deleted_at is null  "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix = "با"
    prefix2 = "نە"
    k = 1
    for i in result:
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0])
        print(k)
        k += 1
        print(word.base)
        # print(word.type_tepar)
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn(
            "t")
        barkars = generate_sada_tofiq.getPronounn("tn")

        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            if j2[0] != '3':
                                continue
                        if i[13] != 1:
                            bbk = ""
                        if word.base.count("_") == 1:
                            g_word = word.replace(word.base, '_', prefix + pronouns[j] + bbk, 0)
                        else:
                            g_word = word.replace(word.base, '_', pronouns[j], 0)
                            g_word = word.replace(g_word, '_', prefix + bbk, -1)
                    else:
                        if word.base.count("_") == 1:
                            g_word = word.replace(word.base, '_', prefix + pronouns[j], -1)
                        else:
                            g_word = word.replace(word.base, "_", prefix + pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))
        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            if j2[0] != '3':
                                continue
                        if i[13] != 1:
                            bbk = ""
                        if word.base.count("_") == 1:
                            g_word = prefix2 + pronouns[j] + word.replace(word.base, '_', prefix+bbk, 0)
                        elif word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', pronouns[j] + prefix2, 0)
                            g_word = word.replace(g_word, '_', prefix+bbk, -1)
                        else:
                            g_word = word.replace(word.base, '_', pronouns[j], 0)
                            g_word = word.replace(g_word, '_', prefix2, -2)
                            g_word = word.replace(g_word, '_', prefix+bbk, -1)
                    else:
                        if word.base.count("_") == 1:
                            g_word = prefix2 + word.replace(word.base, '_', prefix + pronouns[j], -1)
                        else:
                            g_word = word.replace(word.base, '_', prefix2, 0)
                            g_word = word.replace(g_word, '_', prefix + pronouns[j], -1)

                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save==1:
            word.saveword(tenses, "ڕابردووی نزیکی دانانی", 0, "لێکدراو")
            word.saveword(neg_tenses, "ڕابردووی نزیکی دانانی", 1, "لێکدراو")


def rabrdui_bardawami_danani(negative="np",sql="",save=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='l' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix = "ب"
    prefix2 = "نە"
    postfix = "ایە"
    k =1
    for i in result:
        print(k)
        k+=1
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0])
        print(word.base)
        # print(word.type_tepar)
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn(
            "t")
        barkars = generate_sada_tofiq.getPronounn("tn")
        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            if j2[0] != '3':
                                continue
                        if word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', pronouns[j] + prefix, 0)
                            if i[13] == 1:
                                g_word = word.replace(g_word, '_', bbk + postfix, -1)
                            else:
                                g_word = word.replace(g_word, '_', postfix, -1)
                        else:
                            g_word = word.replace(word.base, '_', pronouns[j], 0)
                            g_word = word.replace(g_word, '_', prefix, -2)
                            if i[13] == 1:
                                g_word = word.replace(g_word, '_', bbk + postfix, -1)
                            else:
                                g_word = word.replace(g_word, '_', postfix, -1)
                    else:
                        g_word = word.replace(word.base, "_", prefix, -2)
                        g_word = word.replace(g_word, "_", pronouns[j] + postfix, -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))
        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            if j2[0] != '3':
                                continue
                        if word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', pronouns[j] + prefix2, 0)
                            if i[13] == 1:
                                g_word = word.replace(g_word, '_', bbk + postfix, -1)
                            else:
                                g_word = word.replace(g_word, '_', postfix, -1)
                        else:
                            g_word = word.replace(word.base, '_', pronouns[j], 0)
                            g_word = word.replace(g_word, '_', prefix2, -2)
                            if i[13] == 1:
                                g_word = word.replace(g_word, '_', bbk + postfix, -1)
                            else:
                                g_word = word.replace(g_word, '_', postfix, -1)
                    else:
                        g_word = word.replace(word.base, "_", prefix2, -2)
                        g_word = word.replace(g_word, "_", pronouns[j] + postfix, -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save==1:
            word.saveword(tenses, "ڕابردووی بەردەوامی دانانی", 0, "لێکدراو")
            word.saveword(neg_tenses, "ڕابردووی بەردەوامی دانانی", 1, "لێکدراو")


def rabrdui_duri_danani(negative="np",sql="",save=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='l' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix = "بوو"
    prefix2 = "نە"
    postfix = "با"
    postfix2 = "ایە"
    k =1
    barkars = generate_sada_tofiq.getPronounn("tn")

    for i in result:
        print(k)
        k+=1
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0])
        print(word.base)
        # print(word.type_tepar)
        # print(word.cleanit(word.base))
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn(
            "t")

        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            if j2[0] != '3':
                                continue
                        if i[13] != 1:
                            bbk = ""
                        if word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', pronouns[j], 0)
                            g_word = word.replace(g_word, '_', prefix + postfix + bbk + postfix2, -1)
                        else:
                            g_word = word.replace(word.base, '_', pronouns[j], 0)
                            g_word = word.replace(g_word, '_', prefix + postfix + bbk + postfix2, -1)
                    else:
                        g_word = word.replace(word.base, '_', prefix + postfix + pronouns[j] + bbk + postfix2, -1)

                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))
        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ""
                        if word.base.count("_") == 2:
                            g_word = word.replace(word.base, '_', pronouns[j] + prefix2, 0)
                            g_word = word.replace(g_word, '_', prefix + postfix + bbk + postfix2 , -1)
                        else:
                            g_word = word.replace(word.base, '_', pronouns[j], 0)
                            g_word = word.replace(g_word, '_', prefix2, -2)
                            g_word = word.replace(g_word, '_', prefix + postfix + bbk + postfix2 , -1)
                    else:
                        g_word = word.replace(word.base, "_", prefix2, -2)
                        g_word = word.replace(g_word, "_", prefix + postfix + pronouns[j] + bbk + postfix2, -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save==1:
            word.saveword(tenses, "ڕابردووی دووری دانانی", 0, "لێکدراو")
            word.saveword(neg_tenses, "ڕابردووی دووری دانانی", 1, "لێکدراو")


def rabrdui_bkarnadyar(negative="np",sql="",save=0):
    # Rabrduy krdary duri danani 5.d
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='l' and type_tepar='t' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1
    k =1
    words = []
    prefix1 = "نە"
    postfix = "را"
    for i in result:
        print(k)
        k += 1
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0])
        print(word.word)
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn("tn")

        if negative.find('p') != -1:
            for j in pronouns:
                if word.root2 is not None and len(word.root2) > 0:
                    if word.endswith(word.root2, ['ر'], ['_']):
                        postfix = 'ا'
                    else:
                        postfix = 'را'
                    g_word = word.replace(word.root2, '_', postfix + pronouns[j], -1)
                else:
                    if word.endswith(word.root, ['ر'], ['_']):
                        postfix = 'ا'
                    else:
                        postfix = 'را'
                    g_word = word.replace(word.root, '_', postfix + pronouns[j], -1)
                g_word = word.cleanit(g_word)
                if g_word not in all_words:
                    all_words.append(g_word)
                    tenses.append((g_word,'',j))

        if negative.find('n') != -1:
            for j in pronouns:
                if word.root2 is not None and len(word.root2) > 0:
                    if word.endswith(word.root2, ['ر'], ['_']):
                        postfix = 'ا'
                    else:
                        postfix = 'را'
                    g_word = word.replace(word.root2, '_', prefix1, -2)
                    g_word = word.replace(g_word, '_', postfix + pronouns[j], -1)
                else:
                    if word.endswith(word.root, ['ر'], ['_']):
                        postfix = 'ا'
                    else:
                        postfix = 'را'
                    g_word = word.replace(word.root, '_', prefix1, -2)
                    g_word = word.replace(g_word, '_', postfix + pronouns[j], -1)
                g_word = word.cleanit(g_word)
                if g_word not in all_words:
                    all_words.append(g_word)
                    neg_tenses.append((g_word,'',j))
        # print(tenses)
        if save==1:
            word.saveword(tenses, "ڕابردووی بکەر نادیار", 0, "لێکدراو")
            word.saveword(neg_tenses, "ڕابردووی بکەر نادیار", 1, "لێکدراو")


getallgeneratedwords( )
# saveit = 1
