import json
import re
from Word import Word
import generate_sada_tofiq as gs
from mysql.connector.constants import ClientFlag
import mysql.connector

# mydb = mysql.connector.connect(
#   host="62.201.215.205",
#   user="testuser",
#   password="nlp!3£$%5ADm",
#   database="nlp"
# )

# mydb = mysql.connector.connect(
#   host="62.201.215.205",
#   user="testuser",
#   password="nlp!3£$%5ADm",
#   database="nlp"
# )
mydb = mysql.connector.connect(
    host="mysql.gb.stackcp.com",
    user="torbend-3231318204",
    password="vxwhyj8shy",
    database="torbend-3231318204",
    port="59054"
)


def generate_rabrdooy_tawaw(status="pn",sql="", saveit=0):
  mycursor = mydb.cursor()
  mycursor.execute("select * from vbgs where type='l' and deleted_at is null "+sql)
  result = mycursor.fetchall()  # fetches all the rows in a result set
  x = 1;
  words = []
  aspect1 = "و"
  aspect2 = "وو"
  aspect3 = "ی"
  p_aspect1 = "ەت"
  p_aspect2 = "ە"
  neg_prefix = "نە"
  print("ڕابردووی تەواو")
  for i in result:

    word = Word(i[1], i[3], i[6], i[7], i[8], _id_vbgs=i[0])
    neg_tenses = []

    # print(x, ":", word.word, ":", i[0])
    print(word.base)
    # print(word.root)
    # print(word.type_tepar)

    # print(word)
    # print("ڕابردووی تەواو")
    tenses = []

    pronoun = gs.getPronounn(word.type_tepar)
    barkars = gs.getPronounn("tn")
    bar_kar = ""

    for j2 in barkars:
      for j in pronoun:
        g_word = ""
        if status == "p" or status == "pn":  # are
          if word.type_tepar == "t":  # tepar
            if j2[0] != j[0]:
              bbk = barkars[j2]
            else:
              continue
            if i[13] != 1:
              bbk = ''
            underscore_index = [m.start() for m in re.finditer("_", word.base)]
            char_befor_underscore = word.base[underscore_index[-1] - 1]
            pashgr = word.base.endswith("_")
            if pashgr:  # pashgr nabet
              if char_befor_underscore == "ا" or char_befor_underscore == "ی":
                g_word = word.replace(word.base, '_', pronoun[j], 0)
                g_word = word.replace(g_word, '_', aspect1+bbk + p_aspect2, -1)
              elif char_befor_underscore == "د" or char_befor_underscore == "ت":
                g_word = word.replace(word.base, '_', pronoun[j], 0)
                g_word = word.replace(g_word, '_', aspect2+bbk + p_aspect2, -1)
              elif char_befor_underscore == "و":
                g_word = word.replace(word.base, '_', pronoun[j], 0)
                g_word = word.replace(g_word, '_', bbk+p_aspect2, -1)
            else:  # pashgr habet
              g_word = word.replace(word.base, '_', pronoun[j], 0)
              if char_befor_underscore == "ا" or char_befor_underscore == "ی":
                g_word = word.replace(g_word, '_', aspect1+bbk+p_aspect1, -1)
              elif char_befor_underscore == "د" or char_befor_underscore == "ت":
                g_word = word.replace(g_word, '_', aspect2+bbk + p_aspect1, -1)
          else:  # tenapar
            underscore_index = [m.start() for m in re.finditer("_", word.base)]
            char_befor_underscore = word.base[underscore_index[-1] - 1]
            pashgr = word.base.endswith("_")
            if pashgr:  # pashgr nabet
              pro = pronoun[j]
              if j == "3t":
                pro = "ە"
              if char_befor_underscore == "ا" or char_befor_underscore == "ی":
                g_word = word.replace(word.base, '_', aspect1 + pro, -1)
              elif char_befor_underscore == "د" or char_befor_underscore == "ت":
                g_word = word.replace(word.base, '_', aspect2 + pro, -1)
              elif char_befor_underscore == "و":
                g_word = word.replace(word.base, '_', pro, -1)
            else:  # pashgr habet
              if char_befor_underscore == "و":
                g_word = word.replace(word.base, '_', pronoun[j] + p_aspect1, -1)
              else:
                g_word = word.replace(word.base, '_', aspect1 + pronoun[j] + p_aspect1, -1)
          if g_word != "":
            ww = word.cleanit(g_word)
            if not [item for item in tenses if ww in item]:
              if i[13] == 1:
                bar_kar = j2
              tenses.append((ww, j, bar_kar))
        if status == "n" or status == "pn":  # nareni
          if word.type_tepar == "t":  # tepar
            if j2[0] != j[0]:
              bbk = barkars[j2]
            else:
              continue
            if i[13] != 1:
              bbk = ''
            underscore_index = [m.start() for m in re.finditer("_", word.base)]
            char_befor_underscore = word.base[underscore_index[-1] - 1]
            pashgr = word.base.endswith("_")
            if word.base.count("_") == 2:
              nu = 2
            elif word.base.count("_") == 3:
              nu = 3
            if pashgr:  # pashgr nabet
              if char_befor_underscore == "ا" or char_befor_underscore == "ی":
                if nu == 2:
                  g_word = word.replace(word.base, '_', pronoun[j] + neg_prefix, 0)
                elif nu >= 3:
                  g_word = word.replace(word.base, '_', neg_prefix, -2)
                  g_word = word.replace(g_word, '_', pronoun[j], 0)
                g_word = word.replace(g_word, '_', aspect1+bbk + p_aspect2, -1)
              elif char_befor_underscore == "د" or char_befor_underscore == "ت":
                if nu == 2:
                  g_word = word.replace(word.base, '_', pronoun[j] + neg_prefix, 0)
                elif nu >= 3:
                  g_word = word.replace(word.base, '_', neg_prefix, -2)
                  g_word = word.replace(g_word, '_', pronoun[j], 0)
                g_word = word.replace(g_word, '_', aspect2+bbk + p_aspect2, -1)
              elif char_befor_underscore == "و":
                if nu == 2:
                  g_word = word.replace(word.base, '_', pronoun[j] + neg_prefix, 0)
                elif nu >= 3:
                  g_word = word.replace(word.base, '_', neg_prefix, -2)
                  g_word = word.replace(g_word, '_', pronoun[j], 0)
                g_word = word.replace(g_word, '_', bbk+p_aspect2, -1)
            else:  # pashgr habet
              if nu == 2:
                g_word = word.replace(word.base, '_', pronoun[j] + neg_prefix, 0)
              elif nu >= 3:
                g_word = word.replace(word.base, '_', neg_prefix, -2)
                g_word = word.replace(g_word, '_', pronoun[j], 0)
              if char_befor_underscore == "ا" or char_befor_underscore == "ی":
                g_word = word.replace(g_word, '_', aspect3+bbk, -1)
          else:  # tenapar nare
            underscore_index = [m.start() for m in re.finditer("_", word.base)]
            char_befor_underscore = word.base[underscore_index[-1] - 1]
            pashgr = word.base.endswith("_")
            if pashgr:  # pashgr nabet
              pro = pronoun[j]
              g_word = word.replace(word.base, '_', neg_prefix, -2)
              if j == "3t":
                pro = "ە"
              if char_befor_underscore == "ا" or char_befor_underscore == "ی":
                g_word = word.replace(g_word, '_', aspect1 + pro, -1)
              elif char_befor_underscore == "د" or char_befor_underscore == "ت":
                g_word = word.replace(g_word, '_', aspect2 + pro, -1)
              elif char_befor_underscore == "و":
                g_word = word.replace(g_word, '_', pro, -1)
            else:  # pashgr habet
              g_word = word.replace(word.base, '_', neg_prefix, -2)
              if char_befor_underscore == "و":
                g_word = word.replace(g_word, '_', pronoun[j] + p_aspect1, -1)
              else:
                g_word = word.replace(g_word, '_', aspect1 + pronoun[j] + p_aspect1, -1)
          if g_word != "":
            ww = word.cleanit(g_word)
            if not [item for item in neg_tenses if ww in item]:
              if i[13] == 1:
                bar_kar = j2
              neg_tenses.append((ww, j, bar_kar))
    if saveit == 1:
      word.saveword(tenses, "ڕابردووی تەواو", 0, "لێکدراو")
      word.saveword(neg_tenses, "ڕابردووی تەواو", 1, "لێکدراو")
    print(tenses)
    print(neg_tenses)
# generate_rabrdooy_tawaw('n')

def generate_rabrdooy_tawaw_danani(status="pn",sql="", saveit=0):
  mycursor = mydb.cursor()
  mycursor.execute("select * from vbgs where type='l' and deleted_at is null "+sql)
  result = mycursor.fetchall()  # fetches all the rows in a result set
  x = 1;
  words = []
  aspect1 = "بێت"
  aspect2 = "وو"
  aspect3 = "ی"
  p_aspect1 = "ەت"
  p_aspect2 = "ە"
  neg_prefix = "نە"
  print("ڕابردووی تەواوی دانانی")
  for i in result:

    word = Word(i[1], i[3], i[6], i[7], i[8], _id_vbgs=i[0])
    neg_tenses = []

    # print(x, ":", word.word, ":", i[0])
    # print(word.base)
    # print(word.root)
    # print(word.type_tepar)

    # print(word)
    # print("ڕابردووی تەواوی دانانی")
    tenses = []

    pronoun = gs.getPronounn(word.type_tepar)
    barkars = gs.getPronounn("tn")
    bar_kar = ""

    for j2 in barkars:
      for j in pronoun:
        g_word = ""
        if status == "p" or status == "pn":  # are
          if word.type_tepar == "t":  # tepar
            if j2[0] != j[0]:
              bbk = barkars[j2]
            else:
              continue
            if i[13] != 1:
              bbk = ''
            g_word = word.replace(word.base, '_', pronoun[j], 0)
            g_word = word.replace(g_word, '_', aspect1+bbk, -1)
          else:  # tenapar
            g_word = word.replace(word.base, '_', aspect1 + pronoun[j], -1)
          if g_word != "":
            ww = word.cleanit(g_word)
            if not [item for item in tenses if ww in item]:
              if i[13] == 1:
                bar_kar = j2
              tenses.append((ww, j, bar_kar))
        if status == "n" or status == "pn":  # nareni
          if word.type_tepar == "t":  # tepar
            if word.base.count("_") == 2:
              g_word = word.replace(word.base, '_', pronoun[j] + neg_prefix, 0)
            elif word.base.count("_") == 3:
              g_word = word.replace(word.base, '_', neg_prefix, -2)
              g_word = word.replace(g_word, '_', pronoun[j], 0)
            g_word = word.replace(g_word, '_', aspect1, -1)
          else:  # tenapar
              g_word = word.replace(word.base, '_', neg_prefix, -2)
              g_word = word.replace(g_word, '_', aspect1 + pronoun[j], -1)
          if g_word != "":
            ww = word.cleanit(g_word)
            if not [item for item in neg_tenses if ww in item]:
              if i[13] == 1:
                bar_kar = j2
              neg_tenses.append((ww, j, bar_kar))

    if saveit ==1:
      word.saveword(tenses, "ڕابردووی تەواوی دانانی", 0, "لێکدراو")
      word.saveword(neg_tenses, "ڕابردووی تەواوی دانانی", 1, "لێکدراو")
    print(tenses)
    print(neg_tenses)

# generate_rabrdooy_tawaw_danani('p')

def generate_wistiyu_marji(status="pn",sql="", saveit=0):
  mycursor = mydb.cursor()
  mycursor.execute("select * from vbgs where type='l' and deleted_at is null "+sql)
  result = mycursor.fetchall()  # fetches all the rows in a result set
  x = 1;
  words = []
  aspect1 = "ب"
  aspect2 = "وو"
  aspect3 = "ی"
  p_aspect1 = "ەت"
  p_aspect2 = "ە"
  neg_prefix = "نە"
  print("ڕابردووی ویستوئارەزوو و مەرجیی")
  for i in result:

    word = Word(i[1], i[3], i[6], i[7], i[8], _id_vbgs=i[0])
    neg_tenses = []

    # print(x, ":", word.word, ":", i[0])
    # print(word.base)
    # print(word.root)
    # print(word.type_tepar)

    # print(word)
    # print("ویستی و مەرجی ")
    tenses = []

    pronoun = gs.getPronounn(word.type_tepar)


    for j in pronoun:
      g_word = ""
      if status == "p" or status == "pn":  # are
        g_word = word.replace(word.base, '_', aspect1 + pronoun[j], -1)
        if g_word != "":
          ww = word.cleanit(g_word)
          if not [item for item in tenses if ww in item]:
            tenses.append((ww, j, ''))
      if status == "n" or status == "pn":  # nareni
        if word.type_tepar == "t":  # tepar
            if word.base.count("_") == 2:
              g_word = word.replace(word.base, '_', neg_prefix, 0)
            elif word.base.count("_") == 3:
              g_word = word.replace(word.base, '_', neg_prefix, -2)
              # g_word = word.replace(g_word, '_', pronoun[j], 0)
            g_word = word.replace(g_word, '_', aspect1 + pronoun[j], -1)
        else:  # tenapar
            g_word = word.replace(word.base, '_', neg_prefix, -2)
            g_word = word.replace(g_word, '_', aspect1 + pronoun[j], -1)
        if g_word != "":
          ww = word.cleanit(g_word)
          if not [item for item in neg_tenses if ww in item]:
            neg_tenses.append((ww, j, ''))

    if saveit ==1:
      word.saveword(tenses, "ویستی و مەرجی", 0, "لێکدراو")
      word.saveword(neg_tenses, "ویستی و مەرجی", 1, "لێکدراو")
    print(tenses)
    print(neg_tenses)

#
# generate_rabrdooy_tawaw()
# generate_rabrdooy_tawaw_danani()
# generate_wistiyu_marji()