import json
import re
from Word import Word

import mysql.connector


mydb = mysql.connector.connect(
    host="mysql.gb.stackcp.com",
    user="torbend-3231318204",
    password="vxwhyj8shy",
    database="torbend-3231318204",
    port="59054"
)
all_words = []
def getallgeneratedwords():
    mycursor = mydb.cursor()
    mycursor.execute("select * from generated_words  ")
    result = mycursor.fetchall()  # fetches all the rows in a result set
    for i in result:
        all_words.append(i[1])
    print(len(all_words))

def getPronounn_ranbrdu(type_tepar):
    pronoun_tenapar= '{"1t":"م"' \
                     ',"1k":"مان"' \
                     ',"2t":"ت"' \
                     ',"2k": "تان"' \
                     ',"3t": "ی"' \
                     ',"3k":"یان"}'
    pronoun_tepar = '{"1t": "م"' \
                    ',"1k": "ین"' \
                    ',"2t": "یت"' \
                    ',"2k": "ن"' \
                    ',"3t": "ێت",' \
                    '"3k": "ن"}'
    pronouns = pronoun_tenapar if type_tepar == "tn" else pronoun_tepar
    pronoun_dict = json.loads(pronouns)
    return pronoun_dict

def generate_ranabrdooy_sada(status="pn",sql="",saveit=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1;
    words = []
    for i in result:
        word = Word(i[1], i[3], i[6], i[7], i[8],_id_vbgs=i[0])
        print(x, ":", word.word, ":", i[0])
        # print(word.base)
        print(word.root)
        print(word.type_tepar)

        # print(word)
        print("ڕانەبردوو")
        tenses = []
        neg_tenses = []
        morphem = "دە"
        morphem1 = "ئە"
        prefix = "نا"
        pronoun = getPronounn_ranbrdu('t')
        barkars=  getPronounn_ranbrdu('tn')
        bbk=""
        bar_kar = ""

        for j2 in barkars:
            for j in pronoun:
                if status == "p" or status == "pn":
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = morphem + bbk + word.replace(word.root, '_', pronoun[j], -1)
                        g_word1 = morphem1 + bbk + word.replace(word.root, '_', pronoun[j], -1)
                        # g_word = word.replace(word.base, '_', bbk, -1)
                    else:
                        g_word = morphem + word.replace(word.root, '_', pronoun[j], -1)
                        g_word1 = morphem1 + word.replace(word.root, '_', pronoun[j], -1)
                    if g_word != "" or g_word1 != "":
                        ww = word.cleanit(g_word)
                        ww1 = word.cleanit(g_word1)
                        if not [item for item in tenses if ww in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            tenses.append((ww, j, bar_kar))
                        if not [item for item in tenses if ww1 in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            tenses.append((ww1, j, bar_kar))
                if status == "n" or status == "pn":
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = prefix + bbk + word.replace(word.root, '_', pronoun[j], -1)
                    else:
                        g_word = prefix + word.replace(word.root, '_', pronoun[j], -1)
                    if g_word != "":
                        ww = word.cleanit(g_word)
                        if not [item for item in neg_tenses if ww in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            neg_tenses.append((ww, j, bar_kar))
        if saveit == 1:
            word.saveword(tenses, "ڕانەبردووی سادە", 0)
            word.saveword(neg_tenses, "ڕانەبردووی سادە", 1)
        print(tenses)
        print(neg_tenses)

def generate_ranabrdooy_danani_sada(status="pn",sql="",saveit=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1;
    words = []
    for i in result:
        word = Word(i[1], i[3], i[6], i[7], i[8],_id_vbgs=i[0])
        print(x, ":", word.word, ":", i[0])
        # print(word.base)
        print(word.root)
        print(word.type_tepar)

        # print(word)
        print("ڕانەبردووی دانانی")
        tenses = []
        neg_tenses = []
        morphem = "ب"
        morphem1 = "ئە"
        prefix = "نە"
        pronoun = getPronounn_ranbrdu('t')
        barkars=  getPronounn_ranbrdu('tn')
        bbk=""
        bar_kar = ""

        for j2 in barkars:
            for j in pronoun:
                if status == "p" or status == "pn":
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = morphem + bbk + word.replace(word.root, '_', pronoun[j], -1)
                    else: # تێنەپەر
                        g_word = morphem + word.replace(word.root, '_', pronoun[j], -1)
                    if g_word != "" :
                        ww = word.cleanit(g_word)
                        if not [item for item in tenses if ww in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            tenses.append((ww, j, bar_kar))
                if status == "n" or status == "pn":
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = prefix + bbk + word.replace(word.root, '_', pronoun[j], -1)
                    else:
                        g_word = prefix + word.replace(word.root, '_', pronoun[j], -1)
                    if g_word != "":
                        ww = word.cleanit(g_word)
                        if not [item for item in neg_tenses if ww in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            neg_tenses.append((ww, j, bar_kar))
        if saveit == 1:
            word.saveword(tenses, "ڕانەبردووی دانانی", 0)
            word.saveword(neg_tenses, "ڕانەبردووی دانانی", 1)
        print(tenses)
        print(neg_tenses)



# generate_ranabrdooy_sada()
# generate_ranabrdooy_danani_sada()