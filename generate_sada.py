import mysql.connector
from Word import Word
import generate_sada_tofiq

mydb = mysql.connector.connect(
    host="mysql.gb.stackcp.com",
    user="torbend-3231318204",
    password="vxwhyj8shy",
    database="torbend-3231318204",
    port="59054"
)
all_words = []


def getallgeneratedwords():
    mycursor = mydb.cursor()
    mycursor.execute("select * from generated_words  ")
    result = mycursor.fetchall()  # fetches all the rows in a result set
    for i in result:
        all_words.append(i[1])
    print(len(all_words))


def generate_rabrdui_sada(negative="np", sql="", save=0):
    # Rabrduy sada  zhm. 1
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix = "نە"
    for i in result:
        word = Word(i[1], i[3], i[6], i[7], i[8], _id_vbgs=i[0])
        print(word.word)
        print(word.type)
        print(word.base)
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn("t")
        barkars = generate_sada_tofiq.getPronounn("tn")
        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = word.replace(word.base, '_', pronouns[j]+bbk, -1)
                    else:
                        g_word = word.replace(word.base, '_', pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word,j,barkar))

        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = prefix + pronouns[j] + word.replace(word.base, '_', bbk, -1)
                    else:
                        g_word = prefix + word.replace(word.base, '_', pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save == 1 :
            word.saveword(tenses, "ڕابردووی سادە", 0,"سادە")
            word.saveword(neg_tenses, "ڕابردووی سادە", 1,"سادە")


def generate_rabrdui_tawaw(negative="np", sql="", save=0):
    # rabrduy tawaw zhm 3
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1
    words = []
    prefix = "نە"
    aspect = 'ووە'
    # aspect = 'ووە'
    for i in result:
        aspect = "وو"
        word = Word(i[1], i[3], i[6], i[7], i[8], _id_vbgs=i[0])
        print(word.word)
        print(word.type_tepar)
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn("t")
        barkars = generate_sada_tofiq.getPronounn("tn")

        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = word.replace(word.base, '_', aspect + pronouns[j] + bbk, -1)
                    else:
                        j = pronouns[j] if pronouns[j] != "" else "ە"
                        aspect = aspect if word.endswith(word.base, ['ت', 'د'], ['_']) else "و" if word.endswith(word.base,
                                                                                                                 ['ا', 'ی']
                                                                                                                 ,
                                                                                                                 [
                                                                                                                     '_']) else ""
                        g_word = word.replace(word.base, '_', aspect + j, -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))
        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    if j2[0] != j[0]:
                        bbk = barkars[j2]
                    else:
                        continue
                    if i[13] != 1:
                        bbk = ''
                    aspect = 'وو' + bbk + 'ە'
                    if word.type_tepar == "t":
                        aspect = aspect if word.endswith(word.base, ['ت', 'د'], ['_']) else aspect[1:] if word.endswith(word.base,
                                                                                                                  ['ا', 'ی']
                                                                                                                  ,
                                                                                                                  [
                                                                                                                      '_']) else ""

                        g_word = prefix + pronouns[j] + word.replace(word.base, '_', aspect, -1)
                    else:
                        j = pronouns[j] if pronouns[j] != "" else "ە"
                        aspect = aspect if word.endswith(word.base, ['ت', 'د'], ['_']) else "و" if word.endswith(word.base,
                                                                                                                 ['ا', 'ی']
                                                                                                                 ,
                                                                                                                 [
                                                                                                                     '_']) else ""

                        g_word = prefix + word.replace(word.base, '_', aspect[:-1] + j, -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
            print(tenses)
            print(neg_tenses)
            if save == 1:
                word.saveword(tenses,"ڕابردووی تەواو", 0,"سادە")
                word.saveword(neg_tenses,"ڕابردووی تەواو", 1,"سادە")


def generate_rabrdui_krdary_nziki_dananee(negative, sql="", save=0):
    # rabrdui krdari danani nzyky 5.a
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and deleted_at is null"+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1
    words = []
    aspect = "با"
    prefix = "نە"
    for i in result:
        word = Word(i[1], i[3], i[6], i[7], i[8], _id_vbgs=i[0])
        print(word.word)
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn("t")
        barkars = generate_sada_tofiq.getPronounn("tn")
        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if j2[0] != j[0]:
                        bbk = barkars[j2]
                    else:
                        continue
                    if i[13] != 1:
                        bbk = ''
                    g_word = word.replace(word.base, '_', aspect + pronouns[j]+bbk, -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))
                # print(tenses)

        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if j2[0] != j[0]:
                        bbk = barkars[j2]
                    else:
                        continue
                    if i[13] != 1:
                        bbk = ''
                    # j = j if j != "" else "ە
                    if word.type_tepar == "t":
                        g_word = prefix + pronouns[j] + word.replace(word.base, '_', aspect + bbk, -1)
                    else:
                        g_word = prefix + word.replace(word.base, '_', aspect + pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save == 1:
            word.saveword(tenses, "ڕابردووی کرداری نزیکی دانانی", 0,"سادە")
            word.saveword(neg_tenses, "ڕابردووی کرداری نزیکی دانانی", 1,"سادە")


def generate_rabrdui_krdary_tawawe_dananee(negative="np", sql="", save=0):
    # rabrdui krdary tewawy danani 5.b
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1
    words = []
    aspect = "بێت"
    prefix = "نە"
    barkars = generate_sada_tofiq.getPronounn("tn")
    for i in result:
        word = Word(i[1], i[3], i[6], i[7], i[8], _id_vbgs=i[0])
        print(word.word)
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn("t")
        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if j2[0] != j[0]:
                        bbk = barkars[j2]
                    else:
                        continue
                    if i[13] != 1 or word.type_tepar != "t":
                        bbk = ''
                    g_word = word.replace(word.base, '_', aspect + pronouns[j]+bbk, -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))
                # print(tenses)
        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1 :
                            bbk = ''
                        g_word = prefix + pronouns[j] + word.replace(word.base, '_', aspect+bbk, -1)
                    else:
                        g_word = prefix + word.replace(word.base, '_', aspect + pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save == 1:
            word.saveword(tenses, "ڕابردووی کرداری تەواوی دانانی", 0,"سادە")
            word.saveword(neg_tenses, "ڕابردووی کرداری تەواوی دانانی", 1,"سادە")


def generate_rabrdui_krdary_bardawami_dananee(negative="np", sql="", save=0):
    # Rabrduy  krdary bardawami danani 5.c
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1
    words = []
    aspect = "ایە"
    prefix = "ب"
    prefix2 = "نە"
    barkars = generate_sada_tofiq.getPronounn("tn")

    for i in result:
        word = Word(i[1], i[3], i[6], i[7], i[8], _id_vbgs=i[0])
        print(word.word)
        # print(word.type_tepar)
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn("t")

        if negative.find('p') != -1:
            if word.type_tepar == "t":
                aspect = "ایە"  # if word.endswith(word.base, ['ت', 'د', 'و', 'ی'], ['_']) else "یە"
            else:
                aspect = "ایە"
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "tn":
                        aspect = "ایە"  # if pronouns[j] != "" else "یە"
                        g_word = prefix + word.replace(word.base, '_', pronouns[j] + aspect, -1)
                    else:
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = prefix + pronouns[j] + word.replace(word.base, '_', bbk+aspect, -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))
        # print(tenses)
        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "tn":
                        aspect = "ایە" if pronouns[j] != "" else "یە"
                        g_word = prefix2 + word.replace(word.base, '_', pronouns[j] + aspect, -1)
                    else:
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        aspect = "ایە"  # if word.endswith(word.base, ['ت', 'د', 'و', 'ی'], ['_']) else "یە"
                        g_word = prefix2 + pronouns[j] + word.replace(word.base, '_', bbk+aspect, -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save == 1:
            word.saveword(tenses, "ڕابردووی کرداری بەردەوامی دانانی", 0, "سادە")
            word.saveword(neg_tenses, "ڕابردووی کرداری بەردەوامی دانانی", 1, "سادە")


def generate_rabrdui_krdary_dur_dananee(negative="np", sql="", save=0):
    # Rabrduy krdary duri danani 5.d
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and deleted_at is null"+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1
    words = []
    aspect = "ایە"
    prefix1 = "نە"
    prefix = "بووبا"
    barkars = generate_sada_tofiq.getPronounn("tn")

    for i in result:
        word = Word(i[1], i[3], i[6], i[7], i[8], _id_vbgs=i[0])
        print(word.word)
        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn(
            "tn") if word.type_tepar == "tn" else generate_sada_tofiq.getPronounn("t")
        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if j2[0] != j[0]:
                        bbk = barkars[j2]
                    else:
                        continue
                    if i[13] != 1:
                        bbk = ''
                    aspect = "ایە" if pronouns[j] != "" else "یە"
                    g_word = word.replace(word.base, '_', prefix + pronouns[j] + aspect, -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))
        # print(tenses)

        if negative.find('n') != -1:
            aspect = "ایە"
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if word.type_tepar == "tn":
                        aspect = "ایە" if pronouns[j] != "" else "یە"
                        g_word = prefix1 + word.replace(word.base, '_', prefix + pronouns[j] + aspect, -1)
                    else:
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        aspect = "یە"
                        g_word = prefix1 + pronouns[j] + word.replace(word.base, '_', prefix + aspect, -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save == 1:
            word.saveword(tenses, "ڕابردووی کرداری دووری دانانی", 0,"سادە")
            word.saveword(neg_tenses, "ڕابردووی کرداری دووری دانانی", 1,"سادە")


def generate_rabrdui_bkar_nadyar(negative="np", sql="", save=0):
    # Rabrduy krdary duri danani 5.d
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and type_tepar='t' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1
    words = []
    prefix1 = "نە"
    prefix = "را"
    barkars = generate_sada_tofiq.getPronounn("tn")
    for i in result:
        print(i[1])
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0])

        tenses = []
        neg_tenses = []
        pronouns = generate_sada_tofiq.getPronounn("tn")
        if negative.find('p') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if j2[0] != j[0]:
                        bbk = barkars[j2]
                    else:
                        continue
                    if i[13] != 1:
                        bbk = ''
                    if word.root2 is not None and len(word.root2) > 0:
                        g_word = word.replace(word.root2, '_', prefix + pronouns[j], -1)
                    else:
                        g_word = word.replace(word.root, '_', prefix + pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        tenses.append((g_word, j, barkar))

        if negative.find('n') != -1:
            for j2 in barkars:
                for j in pronouns:
                    bbk = ''
                    if j2[0] != j[0]:
                        bbk = barkars[j2]
                    else:
                        continue
                    if i[13] != 1:
                        bbk = ''
                    if word.root2 is not None and len(word.root2) > 0:
                        g_word = prefix1 + word.replace(word.root2, '_', prefix + pronouns[j], -1)
                    else:
                        g_word = prefix1 + word.replace(word.root, '_', prefix + pronouns[j], -1)
                    g_word = word.cleanit(g_word)
                    if g_word not in all_words:
                        barkar = ''
                        if bbk != '':
                            barkar = j2
                        all_words.append(g_word)
                        neg_tenses.append((g_word, j, barkar))
        print(tenses)
        print(neg_tenses)
        if save == 1:
            word.saveword(tenses, "ڕابردووی بکەر نادیار", 0,"سادە")
            word.saveword(neg_tenses, "ڕابردووی بکەر نادیار", 1,"سادە")


def rabrdui_sada(negative="np", sql="", save=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where deleted_at is null and type='s'  "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    prefix_neg = "نە"
    k = 1;
    for i in result:
        print(i[0])
        k +=1
        word = Word(i[1], i[3], i[6], i[7], i[8], i[4], i[5], _id_vbgs=i[0],brga=i[14])

        print(word.brga)
        # print(word.type_tepar)
        tenses = []
        neg_tenses = []

        if negative.find('p') != -1:
           g_word = word.replace(word.base, '_', '', 0)
           g_word = word.cleanit(g_word)
           tenses.append((g_word, '1t', ''))
        if negative.find('n') != -1:
            g_word = prefix_neg + word.replace(word.base, '_', '', 0)
            g_word = word.cleanit(g_word)
            neg_tenses.append((g_word, '1t', ''))

        print(tenses)
        print(neg_tenses)
        if save == 1:
            word.saveword(tenses, "ڕابردووی سادە", 0, 'سادە')
            word.saveword(neg_tenses, " ڕابردووی سادە", 1,'سادە')

getallgeneratedwords()