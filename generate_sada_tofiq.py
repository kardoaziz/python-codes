import json
import re
from Word import Word

import mysql.connector

# mydb = mysql.connector.connect(
#     host="62.201.215.205",
#     user="testuser",
#     password="nlp!3£$%5ADm",
#     database="nlp"
# )
mydb = mysql.connector.connect(
    host="mysql.gb.stackcp.com",
    user="torbend-3231318204",
    password="vxwhyj8shy",
    database="torbend-3231318204",
    port="59054"
)

# pronoun_tepar = ["م", "مان", "ت", "تان", "ی", "یان"]
# pronoun_tenapar = ["م", "ین", "یت", "ن", "", "ن"]


def getPronounn(type_tepar):
    pronoun_tepar = '{"1t":"م"' \
                    ',"1k":"مان"' \
                    ',"2t":"ت"' \
                    ',"2k": "تان"' \
                    ',"3t": "ی"' \
                    ',"3k":"یان"}'
    pronoun_tenapar = '{"1t": "م"' \
                      ',"1k": "ین"' \
                      ',"2t": "یت"' \
                      ',"2k": "ن"' \
                      ',"3t": "",' \
                      '"3k": "ن"}'
    # pronoun_tepar = '{"3t": "ی"' \
    #                 ',"3k":"یان"}'
    # pronoun_tenapar = '{"3t": "",' \
    #                   '"3k": "ن"}'
    pronouns = pronoun_tenapar if type_tepar == "tn" else pronoun_tepar
    pronoun_dict = json.loads(pronouns)
    return pronoun_dict


def generate_rabrdooy_bardawam(status="pn",sql="",saveit=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1;
    words = []
    for i in result:
        word = Word(i[1], i[3], i[6], i[7], i[8],_id_vbgs=i[0])
        print(x, ":", word.word, ":", i[0])
        print(word.base)
        # print(word.root)
        print(word.type_tepar)

        # print(word)
        print("ڕابردووی بەردەوام")
        tenses = []
        neg_tenses = []
        aspect = "دە"
        prefix = "نە"
        pronoun = getPronounn(word.type_tepar)
        barkars=  getPronounn('tn')
        bbk = ""
        bar_kar=""

        for j2 in barkars:
            for j in pronoun:
                if status == "p" or status == "pn":
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = aspect + pronoun[j] + word.replace(word.base, '_', bbk, -1)
                        # g_word = word.replace(word.base, '_', bbk, -1)
                    else:
                        g_word = aspect + word.replace(word.base, '_',  pronoun[j], -1)
                    if g_word != "":
                        ww = word.cleanit(g_word)
                        if not [item for item in tenses if ww in item] :
                            if i[13] == 1:
                                bar_kar= j2
                            tenses.append((ww,j,bar_kar))
                if status == "n" or status == "pn":
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = prefix + pronoun[j] + aspect + word.replace(word.base, '_', bbk, -1)
                    else:
                        g_word = prefix + aspect + word.base + pronoun[j]
                    if g_word != "":
                        ww = word.cleanit(g_word)
                        if not [item for item in neg_tenses if ww in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            neg_tenses.append((ww, j, bar_kar))
        if saveit == 1:
            word.saveword(tenses, "ڕابردووی بەردەوام", 0)
            word.saveword(neg_tenses, "ڕابردووی بەردەوام", 1)
        print(tenses)
        print(neg_tenses)

# generate_rabrdooy_bardawam()

def generate_rabrdooy_door(status="pn",sql="",saveit=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and deleted_at is null "+sql)
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1;
    words = []
    for i in result:

        word = Word(i[1], i[3], i[6], i[7], i[8],_id_vbgs=i[0])

        print(x, ":", word.word, ":", i[0])
        print(word.base)
        # print(word.root)
        print(word.type_tepar)

        # print(word)
        print("ڕابردووی دوور")
        tenses = []
        neg_tenses = []
        aspect = "بوو"
        prefix = "نە"
        pronoun = getPronounn(word.type_tepar)
        barkars = getPronounn('tn')
        bar_kar = ""

        for j2 in barkars:
            for j in pronoun:
                if status == "p" or status == "pn":
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = word.replace(word.base, '_', aspect + pronoun[j]+bbk, -1)
                    else:
                        g_word = word.replace(word.base, '_', aspect + pronoun[j], -1)
                    if g_word != "":
                        ww = word.cleanit(g_word)
                        if not [item for item in tenses if ww in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            tenses.append((ww, j, bar_kar))
                if status == "n" or status == "pn":
                    if word.type_tepar == "t":
                        if j2[0] != j[0]:
                            bbk = barkars[j2]
                        else:
                            continue
                        if i[13] != 1:
                            bbk = ''
                        g_word = prefix + pronoun[j] + word.replace(word.base, '_', aspect+bbk , -1)
                    else:
                        g_word = prefix + word.replace(word.base, '_', aspect + pronoun[j], -1)
                    if g_word != "":
                        ww = word.cleanit(g_word)
                        if not [item for item in neg_tenses if ww in item]:
                            if i[13] == 1:
                                bar_kar = j2
                            neg_tenses.append((ww, j, bar_kar))
        if saveit == 1:
            word.saveword(tenses, "ڕابردووی دوور", 0)
            word.saveword(neg_tenses, "ڕابردووی دوور", 1)
        print(neg_tenses)
        print(tenses)

# generate_rabrdooy_door('p')

def generate_wistw_margi(status="pn",sql="",saveit=0):
    mycursor = mydb.cursor()
    mycursor.execute("select * from vbgs where type='s' and deleted_at is null")
    result = mycursor.fetchall()  # fetches all the rows in a result set
    x = 1;
    words = []

    for i in result:
        word = Word(i[1], i[3], i[6], i[7], i[8],_id_vbgs=i[0])

        # print(x, ":", word.word, ":", i[0])
        print(word.base)
        # print(word.root)
        # print(pronouns)

        # print(word)
        # print("کرداری ویستوئارەزوو و مەرجیی")
        tenses = []
        neg_tenses=[]
        prefix = "نە"
        suffix="ب"
        suffix1="بێت"
        pronoun = getPronounn(word.type_tepar)
        for j in pronoun:
            pronoun[j] = "ێت" if pronoun[j] == "" else pronoun[j]
            if status == "p" or status == "pn":
                if j == "1t":
                    g_word = word.replace(word.base, '_', suffix1 + pronoun[j], -1)
                elif word.type_tepar == "tn":
                    g_word =  word.replace(word.base, '_', suffix1 + pronoun[j], -1)
                elif word.type_tepar == "t":
                    g_word = word.replace(word.base, '_', suffix1 + pronoun[j], -1)
                if g_word != "":
                    ww = word.cleanit(g_word)
                    if not [item for item in tenses if ww in item]:
                        tenses.append((ww, j, ''))
            if status == "n" or status == "pn":
                # if j == "1t":
                #     g_word = prefix +pronoun[j]+ word.replace(word.base, '_', "ب", -1)
                if word.type_tepar == "tn":
                    g_word = prefix + word.replace(word.base, '_', "ب"+pronoun[j], -1)
                elif word.type_tepar == "t":
                    g_word = prefix +pronoun[j]+ word.replace(word.base, '_', "بێت", -1)
                if g_word != "":
                    ww = word.cleanit(g_word)
                    if not [item for item in neg_tenses if ww in item]:
                        neg_tenses.append((ww, j, ''))
        if saveit == 1:
            word.saveword(tenses, "ڕابردووی ویستوئارەزوو و مەرجیی", 0)
            word.saveword(neg_tenses, "ڕابردووی ویستوئارەزوو و مەرجیی", 1)
        print(tenses)
        print(neg_tenses)

# generate_rabrdooy_bardawam()
# generate_rabrdooy_door()
# generate_wistw_margi()
