from klpt.preprocess import Preprocess
import re
import numpy as np
from sklearn.model_selection import train_test_split

preprocessor_ckb = Preprocess("Sorani", "Arabic", numeral="Latin")
import mysql.connector

mydb = mysql.connector.connect(
    host="mysql.gb.stackcp.com",
    user="torbend-3231318204",
    password="vxwhyj8shy",
    database="torbend-3231318204", charset='utf8',
    port="59054")


def getWords(sentence):
    sentence = preprocessor_ckb.normalize(sentence).strip()
    while "  " in sentence:
        sentence = sentence.replace("  ", " ")
    words = sentence.split(" ")
    return words


def getSentences():
    mycursor = mydb.cursor()
    mycursor.execute("select * from sentences  ")
    sentences = mycursor.fetchall()
    return sentences


def getTaggedWords(sentence):
    mycursor = mydb.cursor()
    # print(sentence[1].decode('utf-8'))
    mycursor.execute("""select * from tags where id_sentence= %s """, (sentence[0],))
    words = mycursor.fetchall()
    tags = [a[3] for a in words if a[2].find(" ") == -1]
    words = [a[2] for a in words if a[2].find(" ") == -1]
    return tags, words
    # for word in words:
    #     print(word[2]+":"+word[3])


def extractFeatures(word, index, words):
    return {
        # 'word': word,
        'a': 1 if index == 0 else 0,
        "b": 1 if index == len(words) else 0,
        "c": int(bool((re.match('^(?=.*[0-9]$)(?=.*[a-zA-Z])', word)))),
        "d": 1 if word.startswith('نا') else 0,
        "e": 1 if word.startswith('نە') else 0,
        "f": 1 if word.startswith('ب') else 0,
        "g": 1 if word.startswith('دە') else 0,
        "h": 1 if word.startswith('ئە') else 0,
        "i": 1 if word.startswith('پێ') else 0,
        "j": 1 if word.startswith('تێ') else 0,
        "k": 1 if word.startswith('لێ') else 0,
        "l": 1 if word.endswith('ەکە') else 0,
        "m": 1 if word.endswith('ەوە') else 0,
        "n": 1 if word.endswith('ان') else 0,
        "o": 1 if word.endswith('کان') else 0,
        "p": 1 if word.endswith('ی') else 0,
        "q": 1 if word.endswith('تن') else 0,
        "r": 1 if word.endswith('دن') else 0,
        "s": 1 if word.endswith('م') else 0,
        "t": 1 if word.endswith('ت') else 0,
        # 'prev_word': '' if index == 0 else words[index - 1],
        # 'next_word': '' if index == len(words)-1 else words[index + 1],
        "u": 1 if word.isdigit() else 0
    }


def getFeatures(words):
    features = []
    i = 0
    for word in words:
        f = extractFeatures(word, i, words)
        features.append(f)
        # print(f)
        i += 1
    return features


def getSentenceFeatures(sentence):
    tags, words = getTaggedWords(sentence)
    return getFeatures(words), tags


def train(features, labels):
    X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.2, random_state=20)
    import sklearn_crfsuite
    #
    crf = sklearn_crfsuite.CRF(
        algorithm='lbfgs',
        c1=0.1,
        c2=0.1,
        max_iterations=100,
        all_possible_transitions=True
    )
    crf.fit([X_train], [y_train])
    return crf, X_test, y_test


def predict(crf, X_test):
    return crf.predict([X_test])


def f1_score(y_test, y_pred, crf):
    from sklearn_crfsuite import metrics
    labels = list(crf.classes_)
    return metrics.flat_f1_score(list(map(list, zip(*y_test))), list(map(list, zip(*y_pred))),
                                 average='weighted', labels=labels)


features = []
labels = []
sentences = getSentences()
for sentence in sentences:
    ff, tt = getSentenceFeatures(sentence)
    features.extend(ff)
    labels.extend(tt)

crf, X_test, y_test = train(features, labels)

y_pred = predict(crf,X_test)

print(f1_score(y_test,y_pred,crf))