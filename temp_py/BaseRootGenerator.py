import mysql.connector
mydb = mysql.connector.connect(
  host="localhost",
  database="nlp",
  user="root",
  password="root"
)
mydb2 = mysql.connector.connect(
  host="localhost",
  database="nlp",
  user="root",
  password="root"
)
prefix = ["هەڵ", "لێ", "تێ", "سەرهەڵ", "دا", "پێ", "سورهەڵ", "وەر"]
postfix = ["کردن", "کاندن", "گوتن", "خستن", "هێنان", "کێشان", "کوتان", "سپاردن" ,"کۆشان", "چوون", "بڕین"]
def checkifprefix(word):
  for pr in prefix:
    if(word.startswith(pr)):
      word = pr+"_"+word[-len(pr):]
      return word
      break
  return -1
mycursor=mydb.cursor()
mycursor2=mydb2.cursor()
mycursor.execute("select * from vbgs")
result = mycursor.fetchall()  # fetches all the rows in a result set
for i in result:
  word = i[1]
  base = i[2]
  root = i[3]
  print(word)
  word = word.replace("ک","ک")
  # if(word == 'گۆکردن'):
    # print("gokrdn")
    # print(word.endswith('کردن'))
  ender = ''
  if(word.endswith('ەوە') or word.endswith('ەوه') ):
    ender = 'ەوە'
    word = word[:-3]
    print('remove awa')
    print(word)
  base = word[:-1]+"_"
  if(word.endswith('کردن') or word.endswith('کردن')):
    root =  word.replace("کردن", "_کە_")
    base =  word.replace("کردن", "_کرد_")
  elif (word.endswith('شکاندن')):
    root = word.replace("شکاندن", "_شکێن_")
    base =  word.replace("شکاندن", "_شکاند_")
  elif (word.endswith('شکان')):
    root = word.replace("شکان", "_شکێ_")
    base =  word.replace("شکان", "_شکا_")
  elif (word.endswith('خواردن')):
    root = word.replace("خواردن", "_خۆ_")
    base =  word.replace("خواردن", "_خوارد_")
  elif (word.endswith('گەیشتن')):
    root = word.replace("گەیشتن", "_گە_")
    base =  word.replace("گەیشتن", "_گەیشت_")
  elif (word.endswith('نیشتن')):
    root = word.replace("نیشتن", "_نیش_")
    base =  word.replace("نیشتن", "_نیشت_")
  elif (word.endswith('شاخان')):
    root = word.replace("شاخان", "_شاخێ_")
    base =  word.replace("شاخان", "_شاخا_")
  elif (word.endswith('گێڕان')):
    root = word.replace("گێڕان", "_گێڕ_")
    base =  word.replace("گێڕان", "_گێڕا_")
  elif (word.endswith('گۆڕین')):
    root = word.replace("گۆڕین", "_گۆڕ_")
    base =  word.replace("گۆڕین", "_گۆڕی_")
  elif(word.endswith('بینین')):
    root =  word.replace("بینین", "_بین_")
    base =  word.replace("بینین", "_بینی_")
  elif(word.endswith('گوتن')):
    root =  word.replace("گوتن", "_ڵێ_")
    base =  word.replace("گوتن", "_گوت_")

  elif (word.endswith('بوون')):
    root = word.replace("بوون", "_ب_")
    base =  word.replace("بوون", "_بوو_")
  elif (word.endswith('خستن')):
    root = word.replace("خستن", "_خە_")
    base =  word.replace("خستن", "_خست_")
  elif (word.endswith('دان')):
    root = word.replace("دان", "_دە_")
    base =  word.replace("دان", "_دا_")
  elif (word.endswith('گەڕان')):
    root = word.replace("گەڕان", "_گەڕێ_")
    base =  word.replace("گەڕان", "_گەڕا_")
  elif (word.endswith('گواستن')):
    root = word.replace("گواستن", "_گواز_")
    base =  word.replace("گواستن", "_گواست_")
  elif (word.endswith('گرتن')):
    root = word.replace("گرتن", "_گر_")
    base =  word.replace("گرتن", "_گرت_")
  elif (word.endswith('بەستن')):
    root = word.replace("بەستن", "_بەست_")
    base =  word.replace("بەستن", "_بەست_")
  elif (word.endswith('هێنان') or word.endswith('هێنان')):
    root = word.replace("هێنان", "_هێن_")
    base =  word.replace("هێنان", "_هێنا_")
  elif (word.endswith('کێشان')):
    root = word.replace("کێشان", "_کێش_")
    base =  word.replace("کێشان", "_کيشا_")
  elif (word.endswith('گران')):
    root = word.replace("گران", "_گر_")
    base =  word.replace("گران", "_گرا_")
  elif (word.endswith('زنان')):
    root = word.replace("زنان", "_زنێ_")
    base =  word.replace("زنان", "_زنا_")
  elif (word.endswith('نان')):
    root = word.replace("نان", "_نێ_")
    base =  word.replace("نان", "_نا_")
  elif (word.endswith('خشان')):
    root = word.replace("خشان", "_خشێن_")
    base =  word.replace("خشان", "_خشا_")
  elif (word.endswith('خشاندن')):
    root = word.replace("خشاندن", "_خشێن_")
    base =  word.replace("خشاندن", "_خشاند_")
  elif (word.endswith('خشان')):
    root = word.replace("خشان", "_خشێ_")
    base =  word.replace("خشان", "_خشا_")
  elif (word.endswith('کوتان')):
    root = word.replace("کوتان", "_کوت_")
    base =  word.replace("کوتان", "_کوتا_")
  elif (word.endswith('کۆشان')):
    root = word.replace("کۆشان", "_کۆش_")
    base =  word.replace("کۆشان", "_کۆشا_")
  elif (word.endswith('پڕووکان')):
    root = word.replace("پڕووکان", "_پڕوکێن_")
    base =  word.replace("پڕووکان", "_پڕووکا_")
  elif (word.endswith('گەیاندن')):
    root = word.replace("گەیاندن", "_گەیەن_")
    base =  word.replace("گەیاندن", "_گەیاند_")
  elif (word.endswith('تەقاندن')):
    root = word.replace("تەقاندن", "_تەقێن_")
    base =  word.replace("تەقاندن", "_تەقاند_")
  elif (word.endswith('پەڕاندن')):
    root = word.replace("پەڕاندن", "_پەڕێن_")
    base =  word.replace("پەڕاندن", "_پەڕاند_")
  elif (word.endswith('سەپاندن')):
    root = word.replace("سەپاندن", "_سەپێن_")
    base =  word.replace("سەپاندن", "_سەپاند_")
  elif (word.endswith('پەڕان')):
    root = word.replace("پەڕان", "_پەڕ_")
    base =  word.replace("پەڕان", "_پەڕا_")
  elif (word.endswith('سپاردن')):
    root = word.replace("سپاردن", "_سپێر_")
    base =  word.replace("سپاردن", "_سپارد_")
  elif (word.endswith('خواستن')):
    root = word.replace("خواستن", "_خواز_")
    base =  word.replace("خواستن", "_خواست_")
  elif (word.endswith('کوشتن')):
    root = word.replace("کوشتن", "_کوژ_")
    base =  word.replace("کوشتن", "_کوشت_")
  elif (word.endswith('ژەندن')):
    root = word.replace("ژەندن", "_ژەن_")
    base =  word.replace("ژەندن", "_ژەند_")
  elif (word.endswith('بردن')):
    root = word.replace("بردن", "_بە_")
    base =  word.replace("بردن", "_برد_")

  elif (word.endswith('کەندن')):
    root = word.replace("کەندن", "_کەن_")
    base =  word.replace("کەندن", "_گەند_")
  elif (word.endswith('وەستان')):
    root = word.replace("وەستان", "_وەست_")
    base =  word.replace("وەستان", "_وەستا_")

  elif (word.endswith('پسکان')):
    root = word.replace("پسکان", "_پسکێ_")
    base =  word.replace("پسکان", "_پسکا_")

  elif (word.endswith('گوشین')):
    root = word.replace("گوشین", "_گوش_")
    base =  word.replace("گوشین", "_گوشی_")

  elif (word.endswith('هێشتن')):
    root = word.replace("هێشتن", "_هيڵ_")
    base =  word.replace("هێشتن", "_هێشت_")
  elif (word.endswith('هاویشتن')):
    root = word.replace("هاویشتن", "_هاو_")
    base =  word.replace("هاویشتن", "_هاویشت_")
  elif (word.endswith('ڕۆیشتن')):
    root = word.replace("ڕۆیشتن", "_ڕۆ_")
    base =  word.replace("ڕۆیشتن", "_ڕۆیشت_")
  elif (word.endswith('ڕۆشتن')):
    root = word.replace("ڕۆشتن", "_ڕۆ_")
    base =  word.replace("ڕۆشتن", "_ڕۆشت_")
  elif (word.endswith('گیرسان')):
    root = word.replace("گیرسان", "_گیرسێ_")
    base =  word.replace("گیرسان", "_گیرسا_")
  elif (word.endswith('گیرساندن')):
    root = word.replace("گیرساندن", "_گیرسێن_")
    base =  word.replace("گیرساندن", "_گیرساند_")
  elif (word.endswith('ماڵین')):
    root = word.replace("ماڵین", "_ماڵ_")
    base =  word.replace("ماڵین", "_ماڵی_")
  elif (word.endswith('هاتن')):
    root = word.replace("هاتن", "دێ_")
    base =  word.replace("هاتن", "_هات_")
  elif (word.endswith('شۆردن')):
    root = word.replace("شۆردن", "_شۆ_")
    base =  word.replace("شۆردن", "_شۆرد_")
  elif (word.endswith('بوران')):
    root = word.replace("بوران", "_بورێ_")
    base =  word.replace("بوران", "_بوران_")

  elif (word.endswith('ماڵین')):
    root = word.replace("ماڵین", "_ماڵ_")
    base =  word.replace("ماڵین", "_ماڵی_")
  elif (word.endswith('پژان')):
    root = word.replace("پژان", "_پژێ_")
    base =  word.replace("پژان", "_پژا_")

  elif (word.endswith('مژین')):
    root = word.replace("مژین", "_مژ_")
    base =  word.replace("مژین", "_مژی_")
  elif (word.endswith('بڕان')):
    root = word.replace("بڕان", "_بڕ_")
    base =  word.replace("بڕان", "_بڕا_")
  elif (word.endswith('پێوان')):
    root = word.replace("پێوان", "_پێو_")
    base =  word.replace("پێوان", "_پێوا_")
  elif (word.endswith('خوڕین')):
    root = word.replace("خوڕین", "_خوڕ_")
    base =  word.replace("خوڕین", "_خوری_")
  elif (word.endswith('خلیسکان')):
    root = word.replace("خلیسکان", "_خلیسکێ_")
    base =  word.replace("خلیسکان", "_خلیسکا_")

  elif (word.endswith('سوڕان')):
    root = word.replace("سوڕان", "_سوڕێ_")
    base =  word.replace("سوڕان", "_سوڕا_")
  elif (word.endswith('سوڕمان')):
    root = word.replace("سوڕمان", "_سوڕمێ_")
    base =  word.replace("سوڕمان", "_سوڕما_")

  elif (word.endswith('ناردن')):
    root = word.replace("ناردن", "_نێر_")
    base =  word.replace("ناردن", "_نارد_")
  elif (word.endswith('نووسین')):
    root = word.replace("نووسین", "_نووس_")
    base =  word.replace("نووسی", "_نووسی_")
  elif (word.endswith('هەڵسوون')):
    root = word.replace("هەڵسوون", "هەڵ_سوو_")
    base =  word.replace("هەڵسوون", "هەڵ_سوو_")
  elif (word.endswith('هەڵگرتن')):
    root = word.replace("هەڵگرتن", "هەڵ_گر_")
    base =  word.replace("هەڵگرتن", "_هەڵ_گرت_")
  elif (word.endswith('هەڵسان')):
    root = word.replace("هەڵسان", "هەڵ_س_")
    base =  word.replace("هەڵسان", "هەڵسا_")
  elif (word.endswith('کەوتن')):
    root = word.replace("کەوتن", "_کەو_")
    base =  word.replace("کەوتن", "_کەوت_")
  elif (word.endswith('وتن')):
    root = word.replace("وتن", "_ڵێ_")
    base =  word.replace("وتن", "_وت_")
  elif (word.endswith('بووردن')):
    root = word.replace("بووردن", "_بوور_")
    base =  word.replace("بووردن", "_بوورد_")
  elif (word.endswith('پۆشین')):
    root = word.replace("پۆشین", "_پۆش_")
    base =  word.replace("پۆشین", "_پۆشی_")
  elif (word.endswith('کوشتن')):
    root = word.replace("کوشتن", "_کوژ_")
    base =  word.replace("کوشتن", "_کوشت_")
  elif (word.endswith('هاتن')):
    root = word.replace("هاتن", "_یەت")
    base =  word.replace("هاتن", "_هات_")
  elif (word.endswith('چوون')):
    root = word.replace("چوون", "_چ_")
    base =  word.replace("چوون", "_چوو_")
  elif (word.endswith('بڕین')):
    root = word.replace("بڕین", "_بڕ_")
    base =  word.replace("بڕین", "_بڕی_")
  elif (word.endswith('وەشاندن')):
    root = word.replace("وەشاندن", "_وەشێن_")
    base =  word.replace("وەشاندن", "وەشاند_")
  elif (word.endswith('وەشان')):
    root = word.replace("وەشان", "_وەشێ_")
    base =  word.replace("وەشان", "_وەشا_")
  elif (word.endswith('پسکاندن')):
    root = word.replace("پسکاندن", "_پسکێن_")
    base =  word.replace("پسکاندن", "_پسکاند_")
  elif (word.endswith('مرکاندن')):
    root = word.replace("مرکاندن", "_مرکێن_")
    base =  word.replace("مرکاندن", "_مرکاند_")
  elif (word.endswith('قرتاندن')):
    root = word.replace("قرتاندن", "_قرتێن_")
    base =  word.replace("قرتاندن", "_قرتاند_")
  elif (word.endswith('گەیاندن')):
    root = word.replace("گەیاندن", "_گەیەن_")
    base =  word.replace("گەیاندن", "_گەیاند_")
  elif (word.endswith('شێواندن')):
    root = word.replace("شێواندن", "_شێوێن_")
    base =  word.replace("شێواندن", "_شێواند_")
  elif (word.endswith('شێوان')):
    root = word.replace("شێوان", "_شێوێن_")
    base =  word.replace("شێوان", "_شێوا_")
  elif (word.endswith('لەقاندن')):
    root = word.replace("لەقاندن", "_لەقێن_")
    base =  word.replace("لەقاندن", "_لەقاند_")
  elif (word.endswith('وەشاندن')):
    root = word.replace("وەشاندن", "_وەشێن_")
    base =  word.replace("وەشاندن", "_وەشاند_")
  elif (word.endswith('خلیسکاندن')):
    root = word.replace("خلیسکاندن", "_خلیسکێن_")
    base =  word.replace("خلیسکاندن", "_خلیسکاند_")
  elif (word.endswith('سەنگاندن')):
    root = word.replace("سەنگاندن", "_سەنگێن_")
    base =  word.replace("سەنگاندن", "_سەنگاند_")
  elif (word.endswith('هەڵساندن')):
    root = word.replace("هەڵساندن", "هەڵ_سێن_")
    base =  word.replace("هەڵساندن", "هەڵ_ساند_")
  elif (word.endswith('پەڕاندن')):
    root = word.replace("پەڕاندن", "_پەڕێن_")
    base =  word.replace("پەڕاندن", "_پەڕاند_")
  elif (word.endswith('زڕاندن')):
    root = word.replace("زڕاندن", "_زڕێن_")
    base =  word.replace("زڕاندن", "_زڕاند_")
  elif (word.endswith('خوران')):
    root = word.replace("خوران", "_خورێ_")
    base =  word.replace("خوران", "_خورا_")
  elif(word.endswith('کاندن')):
    root =  word.replace("کاندن", "_کێن_")
    base =  word.replace("کاندن", "_کاند_")
  elif(word.endswith('اندن')):
    word = word[:-2]
    if (word.endswith('ان') and word[:-4] != "ێ"):
      root = "_" + word[:-2] + "ێن_"
    elif (word.endswith('ان') and word[:-4] == "ێ"):
      root = "_" + word[:-2] + "_"

  elif (word.endswith('ان') and word[:-4]!="ێ"):
    root =  "_"+word[:-2]+"ێ_"

  elif (word.endswith('ان') and word[:-4]=="ێ"):
    root = "_"+word[:-2]+"_"




  else:
    root = '_'+word[:-2]+'_'

  if(len(ender)!=0):
    if(base.endswith("ە")):
      base = base +"ی"
    if (base.endswith("ا")):
      base = base + "ی"
    if (base.endswith("ا_")):
      base = base + "ی"
    if (base.endswith("ە_")):
      base = base + "ی"
    base = base + ender
    root = root + ender
  # print(base)
  # print(root)
  # print("-------")
  sql = "UPDATE vbgs SET root='"+root+"', base='"+base+"' WHERE id="+str(i[0])
  print(sql)
  mycursor2.execute(sql)  # Execute SQL Query to update record
  mydb2.commit()
mycursor2.close()
  # break
# mydb.commit(word)

# print(mydb)