import re
import pyodbc
from klpt.preprocess import Preprocess
from klpt.stem import Stem

to_be_removed = ['؛', 'K', 'N',':','-','_','&',')','(','=', '؟','?','!','%','@', '/', '*', 'ص', 'l', 'r', 'm', ';', '$', '€', 'p', 'o', 'u', 'n', 'd', '{', 'ذ', '#', 'E', 'D', 'i', 'k', 'a', 'G', 'e', 's', 'W', 'f', 't', 'R', 'c', 'h', 'H', 'ط', 'U', 'L', 'V', 'O', '+', 'C', 'A', 'M', 'B', 'v', 'b', 'x', 'Ç', 'Y', 'g', 'P', 'ü', 'z', 'S', "'", 'T', 'Z', 'F', '٪', 'ظ', 'I', 'q', 'w', 'y', 'ْ', 'X', 'Q', 'ٲ', '«', '»', '\\', '[', 'ث', 'j', 'ı', '１', '２', '３', '４', '５', '６', '７', 'ء', '>', '<', 'ھ', '٫', 'ض', '\u202c', 'J', 'ê', '۔', '}', '８', '９', '０', 'е', 'о', 'а', '٬', 'Р', 'И', 'А', 'Н', 'в', 'с', 'т', 'и', '👇', '\u202a', 'ä', ']', 'ö', 'İ', '·', 'î', '�', 'ş', '☀', '️', 'ç', 'Ö', 'û', 'Ş', '🙏', '🏽', '\u202b', '🇸', '🇾', 'ٸ', '￼', 'ٔ', '؍', '✅', '`', '~', '٭', '\xad', '|', '¾']

preprocessor_ckb = Preprocess("Sorani", "Arabic", numeral="Latin")
stemmer = Stem("Sorani", "Arabic")

import alphabets
alphas = ["ا","ب","ج","چ","د","ە","ێ","ف","گ","ه","ح","ژ","ک","ل","ڵ","م","ن","ۆ","پ","ق","ر","ڕ","س","ش","ت","وو","ڤ","خ","ز","غ","ع","ئ"," ","و","ی","0","1","2","3","4","5","6","7","8","9","10",'ذ','ص','ض','ھ','ث','ظ','ط']

def cleaning(db,table, index,field_name,update):
    conn = pyodbc.connect('Driver={SQL Server};'
                          'Server=PC2\SQLEXPRESS;'
                          'Database:'+db+';'
                          'Trusted_Connection=yes;')
    conn2 = pyodbc.connect('Driver={SQL Server};'
                          'Server=PC2\SQLEXPRESS;'
                          'DATABASE:'+db+';'
                          'Trusted_Connection=yes;')
    # conn = pyodbc.connect('Driver={SQL Server};'
    #                       'Server=PC2\SQLEXPRESS;'
    #                       'webkurdsat;'
    #                       'Trusted_Connection=yes;')
    # conn2 = pyodbc.connect('Driver={SQL Server};'
    #                        'Server=PC2\SQLEXPRESS;'
    #                        'webkurdsat;'
    #                        'Trusted_Connection=yes;')
    conn2.autocommit = True
    cursor = conn.cursor()
    cursor2 = conn2.cursor()
    sql  = 'select * from [Knn-news-db].[dbo].[tbl_KS_BabetiLekchu] ;'
    # sql  = 'select * from [kurdsat].[dbo].[Content] ;'
    # sql  = 'select * from [webkurdsat].[dbo].[kurdsatnews] ;'
    cursor.execute(sql)

    data =[]
    j=0
    extras = []
    total_extras = []

    for i in cursor:
        try:
            j+=1
            # Get the Content
            a = i[index]
            print(j,"<--")
            # print(a)
            # if(j==1):
            #     continue
                # correct spelling Not Finished due to incomplete dictionary
            # words = a.split()
            # print(words[0])

            a = preprocessor_ckb.normalize(f"{a}")
            # remove url and links in the content
            a = re.sub(r'http\S+', '', a)
            # remove some special characters
            a = a.strip()
            a = a.replace("\n", " ")
            a = a.replace("\r", "")
            # a = a.replace("\"", "")
            while("  " in a):
                a = a.replace("  ", " ")
            # a = a.replace("  ", " ")
            to_be_removed = []
            # # filter the characters and keep only those that are in alphas
            # # add characters in a list
            for l in a:
                if(l not in alphas and l not in to_be_removed):
                    to_be_removed.append(l)
                # if(l not  in alphas and l not in total_extras):
                #     total_extras.append(l)
            # # replacing those characters with space
            for l in to_be_removed:
                a = a.replace(l,"")

            b = preprocessor_ckb.normalize(f"{a}")
            data.append([i[0],b])
            # print(b)
            # if(j==10):
            #     break
        except Exception as e:
            print(e)

    # just print list of those characters that has been removed
    print("total_extras")
    print(total_extras)
    if(update):
        for d in data:
            print("->",d[0])
            cursor2.execute('''update [Knn-news-db].[dbo].[tbl_KS_BabetiLekchu] set Title=? where id=?''', d[1], d[0])
            # cursor2.execute('''update [kurdsat].[dbo].[Content] set Detail=? where id=?''', d[1], d[0])
            # cursor2.execute('''update [webkurdsat].[dbo].[kurdsatnews] set details=? where id=?''', d[1], d[0])
    conn2.commit()
    cursor.close()
    cursor2.close()
    conn.close()
    conn2.close()

