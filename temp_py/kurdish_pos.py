import nltk
import pyodbc as pyodbc
import word_classifier
import re
from klpt.preprocess import Preprocess
preprocessor_ckb = Preprocess("Sorani", "Arabic", numeral="Latin")

regex = re.compile('[@_!#$%^&*()<>?/\|}{~:]')
conn = pyodbc.connect('Driver={SQL Server};'
                          'Server=PC2\SQLEXPRESS;'
                          'Database=allnews;'
                          'Trusted_Connection=yes;')
cursor = conn.cursor()
conn1 = pyodbc.connect('Driver={SQL Server};'
                          'Server=PC2\SQLEXPRESS;'
                          'Database=allnews;'
                          'Trusted_Connection=yes;')
cursor1 = conn1.cursor()

cursor.execute('select word,kurdish_meaning from eng_to_ku')
kword={}
allkwords=[]
j=0
for i in cursor:
    ew=nltk.word_tokenize(i[0])
    if len(ew)<2 and not any(char.isdigit() for char in ew[0]) and regex.search(ew[0]) == None:
        type=nltk.pos_tag(ew)
        # print(type)
        kws=i[1].split('،')
        # print(str(i[0])+'->'+str(kws))
        # kword=[]
        # for n in kws:
        #     if (n not in allkwords):
        #         allkwords.append(n)
        for m in kws:
            m=m.strip()
            m=  preprocessor_ckb.normalize(m)
            ktype = preprocessor_ckb.normalize(word_classifier.tokenize(type[0][1]))
            if (regex.search(m) == None and m not in allkwords):
                kword[m]= ktype
                allkwords.append(m)
                # print('rrrrr')
            elif m in allkwords and  ktype not in kword[m]:

                kword[m] = kword[m]+","+ktype
                break


    j+=1
    print(j)
    # if(j>1000):
    #     break

# print(str(ew)+'->'+str(any(char.isdigit() for char in ew[0])))

for ww in kword:
    # print(ww)
    cursor1.execute('''INSERT INTO kurdishpos (word, type) VALUES(?,?)''',
                   ww, kword[ww])
    conn1.commit()
# print(kword)
# print(len(kword))
conn.close()
conn1.close()
