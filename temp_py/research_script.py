import mysql.connector



# this function is to get total number of words in all typeds news includes correct and misspelled words.
def get_total_number_of_words_in_typed_news():
  mydb = mysql.connector.connect(
    host="localhost",
    database="nlp",
    user="root",
    password="root"
  )
  mycursor=mydb.cursor()
  mycursor.execute("select * from typed_news")
  result = mycursor.fetchall()
  num_words_in_all_typed_news = 0
  for news in result:
    num_words_in_all_typed_news+=len(news[4].strip().split())
    # words = news['content'].strip()
  print("total number of words in all typed news:",num_words_in_all_typed_news)


def createchart(title,xlabels, ylabels, xp, yp,shape,gridy):
  import matplotlib.pyplot as plt
  import numpy as np
  xpoints = np.array(xp)
  ypoints = np.array(yp)
  plt.plot(xpoints, ypoints,shape)
  plt.ylabel(ylabels)
  plt.xlabel(xlabels)

  plt.title(title)
  if(gridy==True):
    plt.grid(axis='y')
  plt.show()
# testchart()

def createbarchart(title,xlabels, ylabels, xp, yp,gridy, clr,w):
  import matplotlib.pyplot as plt
  import numpy as np
  xpoints = np.array(xp)
  ypoints = np.array(yp)
  x = np.arange(0,len(xp))  # print("create a chart")


  # print("create a chart")
  plt.bar(xpoints, ypoints, color=clr,width = w)
  plt.ylabel(ylabels)
  plt.xlabel(xlabels)
  plt.xticks(x)
  plt.title(title)
  if(gridy==True):
    plt.grid(axis='y')
  plt.show()

# get number of errors for Novis and Writers
def num_of_per_type():
  mydb = mysql.connector.connect(
    host="localhost",
    database="nlp",
    user="root",
    password="root"
  )
  mycursor = mydb.cursor()
  mycursor.execute("select  u.name, sum(quantity) from errors e, typed_news tn, users u  where tn.id=e.news_id and tn.id_user=u.id group by u.id ")
  result = mycursor.fetchall()
  num_words_in_all_typed_news = 0
  nov_errors = 0
  writer_errors=0
  for err in result:
    if(err[0].find('ovis')!=-1):
      nov_errors+=err[1]
    else:
      writer_errors+=err[1]
    print(err)

  print("total novis errors:",nov_errors)
  print("total Writer errors: ",writer_errors)

def num_of_single_multiple_errors():
  mydb = mysql.connector.connect(
    host="localhost",
    database="nlp",
    user="root",
    password="root"
  )
  mycursor = mydb.cursor()
  mycursor.execute("select  multiple, sum(quantity) from errors e group by multiple ")
  result = mycursor.fetchall()
  num_words_in_all_typed_news = 0
  nov_errors = 0
  writer_errors=0
  for err in result:
    print(err)

# num_of_single_multiple_errors()
# chart for number of words that has error with respect to its length
def errors_per_wor_length():
  mydb = mysql.connector.connect(
    host="localhost",
    database="nlp",
    user="root",
    password="root"
  )
  mycursor = mydb.cursor()
  mycursor.execute("select  * from errors  ")
  result = mycursor.fetchall()
  no_space_error = 0
  max_word_length = 0
  min_word_length = 0
  extra_space_single_word = 0
  num_just_space_errors = 0
  num_of_errors = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
  for err in result:
    # print(err[4],"->",err[5])
    if(err[4].strip().find(" ")==-1 and err[5].strip().find(" ")==-1):
      num_of_errors[len(err[5].strip())-1]+=err[9]
      no_space_error+=1;
      if(len(err[5].strip())>max_word_length) :
        max_word_length=len(err[5].strip())
        # print(err[5])
      if (len(err[5].strip()) < min_word_length):
        min_word_length = len(err[5].strip())
    elif(err[4].strip().find(" ")!=-1):
      if(err[5].strip().find(" ")==-1):
        # print("a")
        num_of_errors[len(err[5].strip()) - 1] += err[9]
      else:
        # print("b")
        words = err[5].strip().split(" ")
        cc = False
        for w in words:
          if(err[4].strip().find(w)==-1):
            num_of_errors[len(w) - 1] += err[9]
            cc=True
        if(cc==False):
          num_just_space_errors+=1

    elif(err[5].strip().find(" ")!=-1):
      words = err[5].strip().split(" ")
      cc= False
      for w in words:
        if (err[4].strip().find(w) == -1):
          num_of_errors[len(w) - 1] += err[9]
          cc=True
      if (cc == False):
        num_just_space_errors += err[9]
    # elif( err[5].strip().find(" ")==-1 and err[4].strip().find(" ")!=-1 ):
    #   num_of_errors[len(err[5].strip()) - 1] += 1
    #   extra_space_single_word +=1
    #   # print(err[4], "->", err[5])
    # elif(err[5].strip().find(" ")!=-1):
    #   print(err[4], "->", err[5])
  print("number of no space errors:",no_space_error)
  print("max word length:",max_word_length)
  print("min word length:",min_word_length)
  print("single word extra space:",extra_space_single_word)
  print("just space errors:",num_just_space_errors)
  print(num_of_errors)
  total_error = 0
  for e in num_of_errors:
    total_error+=e
  print("total error : ",total_error)
  createbarchart("word length vs # of misspell","word lenth","number of errors",[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22] ,num_of_errors,True,"blue",0.3)

# number of errors per (A,D,S,T) grouped by Novis and Writers
def num_of_ADST_by_type():
  mydb = mysql.connector.connect(
    host="localhost",
    database="nlp",
    user="root",
    password="root"
  )
  mycursor = mydb.cursor()
  mycursor.execute("select  u.name,e.type, sum(e.quantity) from errors e, typed_news tn, users u  where tn.id=e.news_id and tn.id_user=u.id group by u.id,e.type ")
  result = mycursor.fetchall()
  num_words_in_all_typed_news = 0
  total_novis_errors = 0
  total_writer_errors = 0
  nov_errors = [0,0,0,0]
  writer_errors=[0,0,0,0]
  for err in result:
    if(err[0].find('ovis')!=-1 ):
      total_novis_errors+=err[2]
      if(err[1].find("Addition")!=-1):
        nov_errors[0]+=err[2]
      elif(err[1].find("Subtraction")!=-1 ):
        nov_errors[1]+=err[2]
      elif(err[1].find("Substitution")!=-1):
        nov_errors[2]+=err[2]
      elif(err[1].find("Transposition1")!=-1 ):
        nov_errors[3]+=err[2]
    else:
      total_writer_errors+=err[2]
      if (err[1].find("Addition") != -1):
        writer_errors[0] += err[2]
      elif (err[1].find("Subtraction") != -1):
        writer_errors[1] += err[2]
      elif (err[1].find("Substitution") != -1):
        writer_errors[2] += err[2]
      elif (err[1].find("Transposition1") != -1):
        writer_errors[3] += err[2]
  print(" novis errors:",nov_errors)
  print(" Writer errors: ",writer_errors)
  print("total Novis errors: ",total_novis_errors)
  print("total Writer errors: ",total_writer_errors)

def first_position_errors():
  mydb = mysql.connector.connect(
    host="localhost",
    database="nlp",
    user="root",
    password="root"
  )
  mycursor = mydb.cursor()
  mycursor.execute("select  e.id, e.mistake,e.correction,e.quantity, u.name from errors e, typed_news tn, users u  where tn.id=e.news_id and tn.id_user=u.id  ")
  result = mycursor.fetchall()
  first_p_errors = 0
  first_p_novis_errors = 0
  first_p_writer_errors = 0
  no_first_p_errors = 0
  for err in result:
   if(err[1][0]!=err[2][0]):
     first_p_errors+=err[3]
     if(err[4].find('ovis')!=-1):
       first_p_novis_errors+=err[3]
     else:
       first_p_writer_errors+=err[3]
   else:
     no_first_p_errors+=err[3]
  print("first position errors : ",first_p_errors)
  print("first position novice errors : ",first_p_novis_errors)
  print("first position writer errors : ",first_p_writer_errors)
  print("No first position errors : ",no_first_p_errors)

def get_addition_1_by_char():
  mydb = mysql.connector.connect(
    host="localhost",
    database="nlp",
    user="root",
    password="root"
  )
  mycursor = mydb.cursor()
  mycursor.execute("select  * from errors where  multiple is null")
  result = mycursor.fetchall()
  total_addition_error = 0
  # ids = []
  total_uncaught_chars = 0;
  chars_Added = {}
  for err in result:
    if(err[6].find("Addition1")!=-1):
      # ids.append(err[0])
      total_addition_error+=err[9]
      error = err[4].strip()
      correction = err[5].strip()
      i=0
      found = False;
      for c in error:
        err_revmoved_char = error[0: i:] + error[i + 1::]
        i+=1

        if(err_revmoved_char==correction):
          # print(c)
          found=True
          if c in chars_Added:
            chars_Added[c]+=err[9]
          else:
            chars_Added[c]=err[9]
      if(found==False):
        total_uncaught_chars+=err[9]
        print(err[4], "->", err[5])
  # print(chars_Added)
  total_caught_chars = 0;
  for k in chars_Added:
    total_caught_chars+=chars_Added[k]
  print("Total Addition Error ",total_addition_error)
  print("Total Caught Error ",total_caught_chars)
  print("Total UnCaught Error ",total_uncaught_chars)
  print("keys ",list(chars_Added.keys()))
  print("values ",list(chars_Added.values()))
  # print("ids ",ids)
  createbarchart("Addition 1", "characters", "Number of times added", list(chars_Added.keys()),list(chars_Added.values()),False,"blue",0.5)

def get_deletion_1_by_char():
  mydb = mysql.connector.connect(
    host="localhost",
    database="nlp",
    user="root",
    password="root"
  )
  mycursor = mydb.cursor()
  mycursor.execute("select  * from errors where  multiple is null")
  result = mycursor.fetchall()
  total_addition_error = 0
  # ids = []
  total_uncaught_chars = 0;
  chars_Added = {}
  for err in result:
    if(err[6].find("Subtraction1")!=-1):
      # ids.append(err[0])
      total_addition_error+=err[9]
      error = err[4].strip()
      correction = err[5].strip()
      i=0
      found = False;
      for c in correction:
        err_revmoved_char = correction[0: i:] + correction[i + 1::]
        i+=1

        if(err_revmoved_char==error):
          # print(c)
          found=True
          if c in chars_Added:
            chars_Added[c]+=err[9]
          else:
            chars_Added[c]=err[9]
      if(found==False):
        total_uncaught_chars+=err[9]
        print(err[4], "->", err[5])
  # print(chars_Added)
  total_caught_chars = 0;
  for k in chars_Added:
    total_caught_chars+=chars_Added[k]
  print("Total Subtraction Error ",total_addition_error)
  print("Total Caught Error ",total_caught_chars)
  print("Total UnCaught Error ",total_uncaught_chars)
  print("keys ",list(chars_Added.keys()))
  print("values ",list(chars_Added.values()))
  print(chars_Added)
  # print("ids ",ids)
  createbarchart("Subtraction 1", "characters", "Number of times added", list(chars_Added.keys()),list(chars_Added.values()),False,"blue",0.5)


def get_addition_multiple_by_char():
  mydb = mysql.connector.connect(
    host="localhost",
    database="nlp",
    user="root",
    password="root"
  )
  mycursor = mydb.cursor()
  mycursor.execute("select  * from errors where  multiple is null")
  result = mycursor.fetchall()
  total_addition_error = 0
  total_caught_chars = 0;
  total_uncaught_chars = 0;
  chars_Added = {}
  for err in result:
    if(err[6].find("Addition>2")!=-1):
      # ids.append(err[0])
      total_addition_error+=err[9]
      error = err[4].strip()
      correction = err[5].strip()
      i=0
      found = False;
      temp_err = error
      for c in correction:
        i = 0
        for x in temp_err:
          if(c==x):
            temp_err = temp_err[:i] + "_" + temp_err[i+1:]

            break
          i+=1
      for c  in temp_err:
        if(c!="_"):
          if c in chars_Added:
            chars_Added[c] += err[9]
          else:
            chars_Added[c] = err[9]
      total_caught_chars += 1;
  print("Total Caught Error ",total_caught_chars)
  print("keys ",list(chars_Added.keys()))
  print("values ",list(chars_Added.values()))
  # # print("ids ",ids)
  createbarchart("Addition>1", "characters", "Number of times added", list(chars_Added.keys()),list(chars_Added.values()),False,"green",0.5)


def get_deletion_multiple_by_char():
  mydb = mysql.connector.connect(
    host="localhost",
    database="nlp",
    user="root",
    password="root"
  )
  mycursor = mydb.cursor()
  mycursor.execute("select  * from errors where  multiple is null")
  result = mycursor.fetchall()
  total_addition_error = 0
  total_caught_chars = 0;
  total_uncaught_chars = 0;
  chars_Added = {}
  for err in result:
    if(err[6].find("Subtraction>2")!=-1):
      # ids.append(err[0])
      total_addition_error+=err[9]
      error = err[4].strip()
      correction = err[5].strip()
      i=0
      found = False;
      temp_cor = correction
      for c in error:
        i = 0
        for x in temp_cor:
          if(c==x):
            temp_cor = temp_cor[:i] + "_" + temp_cor[i+1:]

            break
          i+=1
      for c  in temp_cor:
        if(c!="_"):
          if c in chars_Added:
            chars_Added[c] += err[9]
          else:
            chars_Added[c] = err[9]
      total_caught_chars += 1;
  print("Total Caught Error ",total_caught_chars)
  print("keys ",list(chars_Added.keys()))
  print("values ",list(chars_Added.values()))
  # # print("ids ",ids)
  createbarchart("Deletion>1", "characters", "Number of times added", list(chars_Added.keys()),list(chars_Added.values()),False,"red",0.5)

# get_deletion_multiple_by_char()
# get_deletion_1_by_char()
# get_addition_1_by_char()
# get_addition_multiple_by_char()
first_position_errors()
# num_of_ADST_by_type()
# errors_per_wor_length()
# testchart()
# createchart("test title","test","number of errors", [2,4,6],[15,33,24],'-',True)
# createbarchart("test title","test","number of errors", [2,4,6],[15,33,24],True,"red",0.3)