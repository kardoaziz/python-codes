def stem(word):

    m = 'م'
    we = 'وە'
    ke = 'کە'
    kan = 'کان'
    ye = 'یە'
    Yk = 'ێک'
    e = 'ە'
    de = 'دە'
    n = 'ن'
    Yt = 'ێت'
    yn = 'ین'
    dn = 'دن'
    tn = 'تن'
    an = 'ان'
    kar = 'کار'
    ga = 'گا'
    tr = 'تر'
    bwwn = 'بوون'
    bww = 'بوو'
    hat = 'هات'
    # srinawae spacekany sarat w kotaeee
    word = word.strip();
    if(word.endswith('گا') or  word.endswith('گە')):
        return word
    # if (len(word) ) > 5 and (word.startswith('ڕا') or word.startswith('لە')) and word not in ('ڕاستگۆ','ڕاکردن'):
    #     word = word[2:]
    # if (len(word) ) > 6 and (word.startswith('هەڵ') or word.startswith('سەر') or word.startswith('دەر')):
    #     word = word[3:]
    if (len(word) ) > 4 and word.endswith('و'):
        word = word[:len(word)-1]
    if (len(word) ) > 4 and word.endswith('دا'):
         word = word[:len(word)-2]
    if (len(word) ) > 4 and word.endswith('ش'):
         word = word[:len(word) - 1]
    if (len(word) ) > 4 and word.endswith('ی'):
        word = word[:len(word) - 1]
    if (len(word) )  > 5 and (word.endswith('یان') or word.endswith('مان') or word.endswith('تان')):
        if not (word.endswith('ستان')):
            word = word[:len(word) - 3]
    if (len(word)) > 6  and word.endswith(we):
        word = word[:len(word) - 2]
    if (len(word) ) > 5 and word.endswith("ەکە"):
        word = word[:len(word) - 3]
    if (len(word) ) > 5 and word.endswith(ke):
        word = word[:len(word) - 2]
    if (len(word) ) > 7 and word.endswith(kan):
        word = word[:len(word) - 3]
    if (len(word) ) > 5 and word.endswith(ye):
        word = word[:len(word) - 2]
    if (len(word) ) > 4 and word.endswith(Yk):
        word = word[:len(word) - 2]
    # if (len(word) ) > 4 and word.endswith(e):
    #     word = word[:len(word) - 1]
    if (len(word) ) > 5 and word.startswith(de) and (word.endswith(n) or word.endswith(m)):
        word = word[2:]
        # word = word[:len(word) - 1]
    if (len(word) ) > 6 and word.startswith(de) and word.endswith(Yt):
        word = word[2:]
        word = word[:len(word) - 1]
    # if (len(word) ) > 5 and (word.endswith(yn) or word.endswith(dn) or word.endswith(tn) or word.endswith(an)):
    #     if(word is not 'زان'  and not word.endswith('ستان')):
    #         word = word[:len(word) - 2]
    if (len(word))  > 4 and word.startswith(kar):
        word = word[2:]
    # if (len(word) )  > 5 and word.endswith(ga):
    #     word = word[:len(word) - 2]
    if (len(word) )  > 5 and word.endswith(tr):
        word = word[:len(word) - 2]
    # if (len(word))  > 6 and word.endswith(bwwn):
    #     word = word[:len(word) - 4]
    # if (len(word) )  > 6 and word.endswith(bww):
    #     word = word[:len(word) - 3]
    # if (len(word) ) > 5 and word.endswith(hat):
    #     word = word[:len(word) - 3]
    return word


# words = ['دیلمان', 'بۆچی', 'دەکات', 'کردمان' ,'کوردستان', 'سەیارەکەم']
# stem('دەکات')
# print(words[0], len(words[0]))
#
# print(words[0].startswith('د'))

# words = ['کوژرا', 'کوژراو', 'کوژراوەکان', 'کوشتی', 'کوشتیان', 'دەکوژرێت', 'کوشت', 'دەکوشت', 'دەکوژرا', 'کوژراوەکانی', 'گواسترانەوە','گواستنەوە', 'گواستن']

# for w in words:
#     print(w, '-> ', stem(w))



