import mysql.connector
from klpt.preprocess import Preprocess
preprocessor_ckb = Preprocess("Sorani", "Arabic", numeral="Latin")

krdar = [
"ڕاكێشان",
"وشكبوون",
"هەوڵدان",
"خەڵەتان",
"ئارەقخواردنەوە",
"حیلاندن",
"ڕاونان",
"وەڕین",
"هەنگاونان",
"تاساندن",
"هەڵشاخان",
"تۆراندن",
"ملدان",
"لێدان",
"فڕاندن",
"ڕابینین",
"لێنان",
"فڕاندن",
"لێدان",
"لێخشاندن",
"فڕین",
"لێخشان",
"ناشتن",
"باڕاندن",
"لەباربردن",
"دابڕان",
"لابردن",
"شاردنەوه",
"كڕكەوتن",
"بوردن",
"كرتاندن",
"دەرهێنان",
"كاردانەوه",
"بزوان",
"قوتدان",
"هەڵنیشتن",
"قۆستنەوه",
"قۆرخكردن",
"دانیشتن",
"هێنان",
"فێڵكردن",
"هەڵسان",
"فڕێدان",
"چەوسانەوه",
"گێڕانەوه",
"دواندن",
"گێڕان",
"دامركاندنەوه",
"گیران",
"داشكان",
"گیانلەدەستدان",
"كشان",
"گوتن",
"گواستنەوه",
"ترازاندن",
"شەكرشكاندن",
"گرێدان",
"تێڕوانیین",
"گڕگرتن",
"پرخاندن",
"گۆكردن",
"هەڵكشان",
"گۆڕین",
"ڕاشەكاندن",
"گۆڕان",
"لێبوردن",
"شنیان",
"تۆقین",
"سەرنجدان",
"ڕاكێشان",
"سەرلێدان",
"خواستن",
"سەردان",
"تاسان",
"سازدان",
"سازان",
"هەڵپڕووكان",
"جووتبوون",
"زانین",
"ڕوان",
"وەشاندن",
"ژنهێنان",
"دڕاندن",
"كۆكین",
"دهستلێدان",
"چاوپێداخشانەوه",
"داڵدەدان",
"هەڵزنان",
"داخوران",
"ڕفان",
"داپڵۆسین",
"پژمین",
"خنكاندن",
"خنكان",
"خەڵەتاندن",
"خشاندن",
"پەرستن",
"خشان",
"خەواندن",
"خزاندن",
"خەوتن",
"خزان",
"ڕفاندن",
"خرۆشاندن",
"ترازان",
"خرۆشان",
"هاویشتن",
"ختووكەدان",
"پەلهاویشتن",
"خولقاندن",
"خەواندن",
"ترساندن",
"پێلێنان",
"داشكاندن",
"پێكدادان",
"پێگوتن",
"شكاندن",
"لێڕابینین",
"پێزانین",
"تۆقاندن",
"پێدان",
"ترنجان",
"پەنادان",
"پەلەكردن",
"پاڵنان",
"خولقان",
"پۆشیین",
"كێشان",
"تێنان",
"داكشان",
"تێدان",
"خوران",
"بوورانەوه",
"ژنخواستن",
"بزواندن",
"گریاندن",
"برەودان",
"گریان",
"بەفیڕۆدان",
"جگەرەكێشان",
"بەردانەوه",
"بەرهەمهێنان",
"بەردان",
"هەڵهێنان",
"بازدان",
"داهێنان",
"بادانەوه",
"بەدهستهێنان",
"بۆلێندن",
"بیستن",
"ئەنجامدان",
"هەڵبڕان",
"ئاوڕدانەوه",
"پژمین",
"ئاگركەوتنەوه",
"بینین",
"ئازاردان"]
mydb = mysql.connector.connect(
  host="localhost",
  database="all_news_all_tables",
  user="root",
  password="root"
)
mydb2 = mysql.connector.connect(
  host="localhost",
  database="nlp",
  user="root",
  password="root"
)
mycursor=mydb.cursor()
mycursor2=mydb2.cursor()
mycursor3=mydb2.cursor()
mycursor.execute("select * from temp_krdar")
result = mycursor.fetchall()  # fetches all the rows in a result set
j = 0
for i in result:
  word = i[1]
  mycursor2.execute("select * from vbgs where word='"+preprocessor_ckb.normalize(i[1].strip())+"'")
  mycursor2.fetchall()
  numrow = mycursor2.rowcount
  if numrow==0 :
    j+=1
    sql = "INSERT INTO vbgs (word) VALUES ('"+preprocessor_ckb.normalize(i[1].strip())+"')"
    # val = ()
    mycursor3.execute(sql)
print("total new vbgs ",j)
# for k in krdar:
#   print(k," : ",preprocessor_ckb.normalize(k))
#   sql = "INSERT INTO temp_krdar (verb, unicode_verb) VALUES (%s, %s)"
#   val = (k, k)
#   mycursor.execute(sql, val)
# result = mycursor.fetchall()  # fetches all the rows in a result set
# for  in result:
#   word = i[1]
#   sql = "UPDATE temp_krdar SET unicode_verb='"+word+"' WHERE id="+str(i[0])
#   mycursor.execute(sql)  # Execute SQL Query to update record
#   break
mydb.commit()
mydb2.commit()
# print(mydb)


