# Connection
import json
import re
from Word import Word
import AllData as data
from temp_py import postagging_using_search as pos
import mysql.connector
from sklearn.feature_extraction.text import TfidfTransformer, TfidfVectorizer, CountVectorizer
import pickle
import pandas as pd
from dilman_py.my_utils import MyUtils as mu

mydb = mysql.connector.connect(
    host="mysql.gb.stackcp.com",
    user="torbend-3231318204",
    password="vxwhyj8shy",
    database="torbend-3231318204",
    port="59054"
)
# load the news all
all_news = []
labels = []
def getNews():
    mycursor = mydb.cursor()
    mycursor.execute("select corrected_content,content from corrected_fake_news ")
    result = mycursor.fetchall()  # fetches all the rows in a result set
    for i in result:
        all_news.append(i[0].decode('utf-8').strip())
        labels.append(0)
        all_news.append(i[1].decode('utf-8').strip())
        labels.append(1)
    print(len(all_news))
    print(len(labels))
getNews()
def tfidf(data):
    print('find tfidf')
    tf1 = pickle.load(open("models/tfidf1.pkl", 'rb'))
    tf = TfidfVectorizer(analyzer='word', ngram_range=(1, 1), max_features=500000,vocabulary=tf1.vocabulary_)
    tf_transformer = tf.fit_transform(data)
    df = pd.DataFrame(tf_transformer[0].T.todense(), index=tf.get_feature_names_out(), columns=["TF-IDF"])
    df = df.sort_values('TF-IDF', ascending=False)
    return df.index[:30].to_numpy()
    # print(df.head(10))
# apply spell correction
# remove stop words

# apply postagging and stemming
i = 0;
for news in all_news:
    print("original")
    # print(all_news[i])
    # print("removed stop words")
    removed_stop_words = ' '.join(["" if j in data.list_of_stop_words else j for j in all_news[i].split()])
    all_news[i] = removed_stop_words
    print(all_news[i])
    print("Verb -> VBGS")
    all_news[i] = pos.verb_to_vbgs(all_news[i])
    print(all_news[i])
    top_30_words=tfidf([all_news[i]])
    w2vect_of_tfidf=[]
    for ww in top_30_words:
        temp=mu.get_similar_tokens(str(ww))
        w2vect_of_tfidf.extend(temp)

    print(w2vect_of_tfidf)
    # print(top_30_words)
    i += 1
    # print(all_news[i-1])
    break




# feature extraction
#     TFIDF 30 words
#     word2vec 5 similar words
#     crossfolding

#     traing
#     testing


